/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "gwmmisc.h"


gboolean
gwm_geometry_intersects_area(GwmGeometry * geometry, GdkRectangle * area)
{
	return TRUE;
}


typedef void
(* GwmSignal_NONE__FLOAT_FLOAT)(GtkObject * object,
				gfloat arg1,
				gfloat arg2,
				gpointer user_data);

void
gwm_marshal_NONE__FLOAT_FLOAT(GtkObject * object,
			      GtkSignalFunc func,
			      gpointer func_data,
			      GtkArg * args)
{
	GwmSignal_NONE__FLOAT_FLOAT rfunc;

	rfunc = (GwmSignal_NONE__FLOAT_FLOAT) func;
	(* rfunc)(object,
		  GTK_VALUE_FLOAT(args[0]),
		  GTK_VALUE_FLOAT(args[1]),
		  func_data);
}
