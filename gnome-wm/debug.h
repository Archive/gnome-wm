/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __debug_h__
#define __debug_h__

#include <glib.h>

/* #define DEBUG 1 */

#if defined DEBUG
#define DEBUG_MESSAGE(format, args...) g_message(format, ##args)
#else
#define DEBUG_MESSAGE(format, args...)
#endif

gchar *debug_xevent_name(gint xevent_type);

#endif /* __debug_h__ */
