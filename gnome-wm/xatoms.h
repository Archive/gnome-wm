/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __xatoms_h__
#define __xatoms_h__

#include <gdk/gdk.h>


/* misc atoms */
extern GdkAtom xa_wm_state;
extern GdkAtom xa_wm_change_state;


/* WM_PROTOCOLS */
extern GdkAtom xa_wm_protocols;
extern GdkAtom xa_wm_take_focus;
extern GdkAtom xa_wm_save_yourself;
extern GdkAtom xa_wm_delete_window;


/* MOTIF WM hint atoms */
extern GdkAtom xa_motif_bindings;
extern GdkAtom xa_motif_wm_hints;
extern GdkAtom xa_motif_wm_messages;
extern GdkAtom xa_motif_wm_offset;
extern GdkAtom xa_motif_wm_menu;
extern GdkAtom xa_motif_wm_info;
extern GdkAtom xa_mwm_hints;
extern GdkAtom xa_mwm_messages;
extern GdkAtom xa_mwm_menu;
extern GdkAtom xa_mwm_info;


/* GNOME WM hint atoms */
extern GdkAtom xa_win_protocols;
extern GdkAtom xa_win_supporting_wm_check;
extern GdkAtom xa_win_client_list;
extern GdkAtom xa_win_wm_name;
extern GdkAtom xa_win_wm_version;
extern GdkAtom xa_win_area;
extern GdkAtom xa_win_area_count;
extern GdkAtom xa_win_icons;
extern GdkAtom xa_win_workspace;
extern GdkAtom xa_win_workspace_count;
extern GdkAtom xa_win_workspace_names;
extern GdkAtom xa_win_workarea;
extern GdkAtom xa_win_layer;
extern GdkAtom xa_win_state;
extern GdkAtom xa_win_hints;
extern GdkAtom xa_win_app_state;
extern GdkAtom xa_win_expanded_size;
extern GdkAtom xa_win_workspace;
extern GdkAtom xa_win_client_moving;

void xatoms_init();

#endif /* __xatoms_h__ */

