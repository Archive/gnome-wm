/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __gwm_context_h__
#define __gwm_context_h__

#include <gtk/gtk.h>

#include "gwmroot.h"
#include "gwmclient.h"
#include "gwmdecoration.h"

#define GWM_TYPE_CONTEXT\
        (gwm_context_get_type())

#define GWM_CONTEXT(obj)\
        (GTK_CHECK_CAST((obj), GWM_TYPE_CONTEXT, GwmContext))

#define GWM_CONTEXT_CLASS(klass)\
        (GTK_CHECK_CLASS_CAST((klass), GWM_TYPE_CONTEXT, GwmContextClass))

#define GWM_IS_CONTEXT(obj)\
        (GTK_CHECK_TYPE((obj), GWM_TYPE_CONTEXT))

#define GWM_IS_CONTEXT_CLASS(klass)\
        (GTK_CHECK_CLASS_TYPE((klass), GWM_TYPE_CONTEXT))

typedef struct _GwmContext GwmContext;
typedef struct _GwmContextClass GwmContextClass;

struct _GwmContext {
	GtkObject object;

	guint32 xid;
	GwmRoot *root;
	GList *client_list;
	GwmClient *focus_client;
};

struct _GwmContextClass {
	GtkObjectClass parent_class;
};

GtkType      gwm_context_get_type(void);
GwmContext * gwm_context_new();
gboolean     gwm_context_manage_xid(GwmContext * context, guint32 xid);
void         gwm_context_capture_existing_windows(GwmContext * context);
void         gwm_context_capture_xid(GwmContext * context, guint32 xid);

#endif /* __gwm_context_h__ */
