/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __gwm_decoration_h__
#define __gwm_decoration_h__

#include <gtk/gtk.h>
#include <gtk/gtkbin.h>
#include "gwmtypes.h"
#include "gwmhints.h"

#define GWM_TYPE_DECORATION\
        (gwm_decoration_get_type())

#define GWM_DECORATION(obj)\
        (GTK_CHECK_CAST((obj), GWM_TYPE_DECORATION, GwmDecoration))

#define GWM_DECORATION_CLASS(klass)\
        (GTK_CHECK_CLASS_CAST((klass), GWM_TYPE_DECORATION, GwmDecorationClass))

#define GWM_IS_DECORATION(obj)\
        (GTK_CHECK_TYPE((obj), GWM_TYPE_DECORATION))

#define GWM_IS_DECORATION_CLASS(klass)\
        (GTK_CHECK_CLASS_TYPE((klass), GWM_TYPE_DECORATION))

typedef enum {
	GWM_DECORATION_STATE_NORMAL,
	GWM_DECORATION_STATE_MOVE,
	GWM_DECORATION_STATE_RESIZE
} GwmDecorationState;

typedef enum {
	GWM_DECORATION_RESIZE_TOP_LEFT,
	GWM_DECORATION_RESIZE_TOP,
	GWM_DECORATION_RESIZE_TOP_RIGHT,
	GWM_DECORATION_RESIZE_RIGHT,
	GWM_DECORATION_RESIZE_BOTTOM_RIGHT,
	GWM_DECORATION_RESIZE_BOTTOM,
	GWM_DECORATION_RESIZE_BOTTOM_LEFT,
	GWM_DECORATION_RESIZE_LEFT
} GwmDecorationResizeState;

typedef struct _GwmDecorationWindows GwmDecorationWindows;
typedef struct _GwmDecorationCursors GwmDecorationCursors;
typedef struct _GwmDecorationMoveInfo GwmDecorationMoveInfo;
typedef struct _GwmDecorationResizeInfo GwmDecorationResizeInfo;
typedef struct _GwmDecoration GwmDecoration;
typedef struct _GwmDecorationClass GwmDecorationClass;

struct _GwmDecorationWindows {
	/* close window */
	GdkWindow *cw;

	/* iconify window */
	GdkWindow *iw;

	/* maximize window */
	GdkWindow *mw;

	/* input-only windows and cursors for resizing;
	 * tl1w = top left 1 window, br2w = bottom right 2 window; 
	 * the corners have two windows so we can maintain a constant
	 * border width around the decoration while extending the corner
	 * region which allows both verticle and horizontal resizing
	 * larger than the resize border width */
	GdkWindow *tl1w, *tl2w, *tw, *tr1w, *tr2w, *rw, *br1w, *br2w,
		*bw, *bl1w, *bl2w, *lw;
};

#define GWM_DECORATION_RESIZE_WINDOWS_COUNT 12

struct _GwmDecorationCursors {
	GdkCursor *tlc, *tc, *trc, *rc, *brc, *bc, *blc, *lc;
};

struct _GwmDecorationMoveInfo {
	gint xoffset, yoffset;
};

struct _GwmDecorationResizeInfo {
	GwmDecorationResizeState state;
	gint xoffset, yoffset, orig_x, orig_y, orig_width, orig_height;
};

struct _GwmDecoration {
	GtkBin bin;

	GwmHints *hints;

	/* state */
	GwmDecorationState state;
	gpointer state_data;

	GwmDecorationWindows windows;
	GwmDecorationCursors cursors;

	/* resize_border_width is the width of the resize border (duh!);
	 * corner_resize_length is the length the corner resize areas
	 * extend down the window borders; titlebar_height is the height
	 * of the title bar part of the decoration */
	gint resize_border_width, corner_resize_length, titlebar_height;
};

struct _GwmDecorationClass {
	GtkBinClass parent_class;

	void (* set_title)                       (GwmDecoration * decoration,
						  gchar * title);
	void (* set_icon_title)                  (GwmDecoration * decoration,
						  gchar * title);
	void (* set_position)                    (GwmDecoration * decoration,
						  gint x,
						  gint y);
	void (* set_min_size)                    (GwmDecoration * decoration,
						  gint min_width,
						  gint min_height);
	void (* set_max_size)                    (GwmDecoration * decoration,
						  gint max_width,
						  gint max_height);
	void (* set_size_incriment)              (GwmDecoration * decoration,
						  gint width_inc,
						  gint height_inc);
	void (* set_aspect_ratio)                (GwmDecoration * decoration,
						  gfloat min_aspect,
						  gfloat max_aspect);
	void (* set_maximized_size)              (GwmDecoration * decoration,
						  gint maximized_with,
						  gint maximized_height);
	void (* child_geometry)                  (GwmDecoration * decoration,
						  GwmGeometry * geometry);
	void (* title_geometry)                  (GwmDecoration * decoration,
						  GwmGeometry * geometry);
	void (* close_button_geometry)           (GwmDecoration * decoration,
						  GwmGeometry * geometry);
	void (* iconify_button_geometry)         (GwmDecoration * decoration,
						  GwmGeometry * geometry);
	void (* maximize_button_geometry)        (GwmDecoration * decoration,
						  GwmGeometry * geometry);
	void (* draw_background)                 (GwmDecoration * decoration,
						  GwmGeometry * geometry,
						  GdkRectangle * area);
	void (* draw_title)                      (GwmDecoration * decoration,
						  GwmGeometry * geometry,
						  GdkRectangle * area);
	void (* draw_close_button)               (GwmDecoration * decoration,
						  GwmGeometry * geometry,
						  GdkRectangle * area);
	void (* draw_iconify_button)             (GwmDecoration * decoration,
						  GwmGeometry * geometry,
						  GdkRectangle * area);
	void (* draw_maximize_button)            (GwmDecoration * decoration,
						  GwmGeometry * geometry,
						  GdkRectangle * area);

	void (* close)                           (GwmDecoration * decoration);
	void (* iconify)                         (GwmDecoration * decoration);
	void (* maximize)                        (GwmDecoration * decoration);
	void (* request_focus)                   (GwmDecoration * decoration);

};

guint      gwm_decoration_get_type               (void);
GwmDecoration* gwm_decoration_new                (GwmHints * hints);
void       gwm_decoration_set_hints              (GwmDecoration * decoration,
						  GwmHints * hints);
void       gwm_decoration_set_title              (GwmDecoration * decoration,
						  gchar * title);
void       gwm_decoration_set_icon_title         (GwmDecoration * decoration,
						  gchar * title);
void       gwm_decoration_set_position           (GwmDecoration * decoration,
						  gint x,
						  gint y);
void       gwm_decoration_set_min_size           (GwmDecoration * decoration,
						  gint min_width,
						  gint min_height);
void       gwm_decoration_set_max_size           (GwmDecoration * decoration,
						  gint max_width,
						  gint max_height);
void       gwm_decoration_set_size_incriment     (GwmDecoration * decoration,
						  gint width_inc,
						  gint height_inc);
void       gwm_decoration_set_aspect_ratio       (GwmDecoration * decoration,
						  gfloat min_aspect,
						  gfloat max_aspect);
void       gwm_decoration_set_maximized_size     (GwmDecoration * decoration,
						  gint maximized_width,
						  gint maximized_height);

void       gwm_decoration_child_geometry         (GwmDecoration * decoration,
						  GwmGeometry * geometry);
void       gwm_decoration_title_geometry         (GwmDecoration * decoration,
						  GwmGeometry * geometry);
void       gwm_decoration_close_button_geometry  (GwmDecoration * decoration,
						  GwmGeometry * geometry);
void       gwm_decoration_iconify_button_geometry(GwmDecoration * decoration,
						  GwmGeometry * geometry);
void       gwm_decoration_maximize_button_geometry(GwmDecoration * decoration,
						  GwmGeometry * geometry);
void       gwm_decoration_draw_background        (GwmDecoration * decoration, 
						  GdkRectangle * area);
void       gwm_decoration_draw_title             (GwmDecoration * decoration, 
						  GdkRectangle * area);
void       gwm_decoration_draw_close_button      (GwmDecoration * decoration, 
						  GdkRectangle * area);
void       gwm_decoration_draw_iconify_button    (GwmDecoration * decoration, 
						  GdkRectangle * area);
void       gwm_decoration_draw_maximize_button   (GwmDecoration * decoration, 
						  GdkRectangle * area);

void       gwm_decoration_close                  (GwmDecoration * decoration);
void       gwm_decoration_iconify                (GwmDecoration * decoration);
void       gwm_decoration_maximize               (GwmDecoration * decoration);
void       gwm_decoration_request_focus          (GwmDecoration * decoration);

#endif /* __gwm_decoration_h__ */
