/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __kde_hints_h__
#define __kde_hints_h__

#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include "gwmhints.h"

void     kde_hints_load                             (GwmHints * hints,
						     Window xwindow);
gboolean kde_hints_process_client_property          (GwmHints * hints,
						     XEvent * xevent);

#endif /* __kde_hints_h__ */
