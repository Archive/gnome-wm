/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* CONVENTION NOTE: except for error trap functions, try to do everything
 *                  in here with Xlib calls and not gdk calls! --JMP 
 */

#include "debug.h"
#include "motif-hints.h"
#include "xatoms.h"


/* MOTIF WM Hints */
typedef struct {
	unsigned long flags;
	unsigned long functions;
	unsigned long decorations;
	long input_mode;
	unsigned long status;
} MotifWmHints, MwmHints;

#define MWM_HINTS_FUNCTIONS     (1L << 0)
#define MWM_HINTS_DECORATIONS   (1L << 1)
#define MWM_HINTS_INPUT_MODE    (1L << 2)
#define MWM_HINTS_STATUS        (1L << 3)

#define MWM_FUNC_ALL            (1L << 0)
#define MWM_FUNC_RESIZE         (1L << 1)
#define MWM_FUNC_MOVE           (1L << 2)
#define MWM_FUNC_MINIMIZE       (1L << 3)
#define MWM_FUNC_MAXIMIZE       (1L << 4)
#define MWM_FUNC_CLOSE          (1L << 5)

#define MWM_DECOR_ALL           (1L << 0)
#define MWM_DECOR_BORDER        (1L << 1)
#define MWM_DECOR_RESIZEH       (1L << 2)
#define MWM_DECOR_TITLE         (1L << 3)
#define MWM_DECOR_MENU          (1L << 4)
#define MWM_DECOR_MINIMIZE      (1L << 5)
#define MWM_DECOR_MAXIMIZE      (1L << 6)

#define MWM_INPUT_MODELESS 0
#define MWM_INPUT_PRIMARY_APPLICATION_MODAL 1
#define MWM_INPUT_SYSTEM_MODAL 2
#define MWM_INPUT_FULL_APPLICATION_MODAL 3
#define MWM_INPUT_APPLICATION_MODAL MWM_INPUT_PRIMARY_APPLICATION_MODAL

#define MWM_TEAROFF_WINDOW	(1L<<0)


/* _MWM_INFO property */
typedef struct {
	long flags;
	Window wm_window;
} MotifWmInfo;
typedef MotifWmInfo MwmInfo;

#define MWM_INFO_STARTUP_STANDARD	(1L<<0)
#define MWM_INFO_STARTUP_CUSTOM		(1L<<1)


/* _MWM_HINTS property */
typedef struct {
	unsigned long flags;
	unsigned long functions;
	unsigned long decorations;
	long          inputMode;
	unsigned long status;
} PropMotifWmHints;
typedef PropMotifWmHints PropMwmHints;

#define PROP_MOTIF_WM_HINTS_ELEMENTS 5
#define PROP_MWM_HINTS_ELEMENTS PROP_MOTIF_WM_HINTS_ELEMENTS


/* _MWM_INFO property, slight return */
typedef struct {
	unsigned long flags;
	unsigned long wmWindow;
} PropMotifWmInfo;
typedef PropMotifWmInfo PropMwmInfo;

#define PROP_MOTIF_WM_INFO_ELEMENTS 2
#define PROP_MWM_INFO_ELEMENTS PROP_MOTIF_WM_INFO_ELEMENTS


static void get_motif_hints(GwmHints * hints, Window xwindow);


void
motif_hints_load(GwmHints * hints, Window xwindow)
{
	get_motif_hints(hints, xwindow);
}


gboolean
motif_hints_process_client_property(GwmHints * hints, XEvent * xevent)
{
	Window xwindow;

	xwindow = xevent->xproperty.window;

	if (xevent->xproperty.atom == xa_motif_wm_hints) {
		get_motif_hints(hints, xwindow);
		return TRUE;
	}

	return FALSE;
}


static void
get_motif_hints(GwmHints * hints, Window xwindow)
{
	guint decoration_flags;
	gint result, actual_format;
	gulong nitems, bytesafter;
	Atom actual_type;
	MotifWmHints *motif_hints;

	DEBUG_MESSAGE("get_motif_hints");

	decoration_flags = 0;

	gdk_error_trap_push();

	result = XGetWindowProperty(
		GDK_DISPLAY(), xwindow, 
		xa_motif_wm_hints, 0L, 20L, False,
		xa_motif_wm_hints, &actual_type, &actual_format, &nitems,
		&bytesafter, (unsigned char **)&motif_hints);
	
	if (gdk_error_trap_pop() || result != Success) {
		DEBUG_MESSAGE("error retriving motif hints");
		return;
	}

	/* I saw this check in FVWM-2, it seems reasonable --JMP */
	if (nitems < PROP_MOTIF_WM_HINTS_ELEMENTS) {
		DEBUG_MESSAGE("not enough motif items");
		XFree(motif_hints);
		return;
	}

	DEBUG_MESSAGE("checking motif hints");

	/* decode the Motif decoration hints */
	if (motif_hints->flags & MWM_HINTS_DECORATIONS) {
		if (motif_hints->decorations & MWM_DECOR_ALL) {
			DEBUG_MESSAGE("all motif decorations");
			decoration_flags = 
				GWM_HINTS_DECOR_BORDER |
				GWM_HINTS_DECOR_RESIZEH |
				GWM_HINTS_DECOR_TITLE |
				GWM_HINTS_DECOR_MENU |
				GWM_HINTS_DECOR_MINIMIZE |
				GWM_HINTS_DECOR_MAXIMIZE;
		}

		if (motif_hints->decorations & MWM_DECOR_BORDER) {
			DEBUG_MESSAGE("motif border decoration");
			decoration_flags ^= GWM_HINTS_DECOR_BORDER;
		}

		if (motif_hints->decorations & MWM_DECOR_RESIZEH)
			decoration_flags ^= GWM_HINTS_DECOR_RESIZEH;

		if (motif_hints->decorations & MWM_DECOR_TITLE)
			decoration_flags ^= GWM_HINTS_DECOR_TITLE;

		if (motif_hints->decorations & MWM_DECOR_MENU)
			decoration_flags ^= GWM_HINTS_DECOR_MENU;

		if (motif_hints->decorations & MWM_DECOR_MINIMIZE)
			decoration_flags ^= GWM_HINTS_DECOR_MINIMIZE;

		if (motif_hints->decorations & GWM_HINTS_DECOR_MAXIMIZE)
			decoration_flags ^= GWM_HINTS_DECOR_MAXIMIZE;
	}

	gwm_hints_set_decoration_flags(hints, decoration_flags);
	XFree(motif_hints);
}
