/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* CONVENTION NOTE: except for error trap functions, try to do everything
 *                  in here with Xlib calls and not gdk calls! --JMP 
 */

#include <X11/Xatom.h>
#include <X11/Xmd.h>

#include "standard-hints.h"
#include "xatoms.h"

static void get_wm_protocols(GwmHints * hints, Window xwindow);
static void get_title(GwmHints * hints, Window xwindow);
static void get_icon_title(GwmHints * hints, Window xwindow);
static void get_normal_hints(GwmHints * hints, Window xwindow);


void
standard_hints_load(GwmHints * hints, Window xwindow)
{
	get_wm_protocols(hints, xwindow);
	get_title(hints, xwindow);
	get_icon_title(hints, xwindow);
	get_normal_hints(hints, xwindow);
}


gboolean
standard_hints_process_client_property(GwmHints * hints, XEvent * xevent)
{
	Window xwindow;
	
	xwindow = xevent->xproperty.window;

	switch (xevent->xproperty.atom) {
	case XA_WM_NAME:
		get_title(hints, xwindow);
		return TRUE;
		break;
	case XA_WM_ICON_NAME:
		get_icon_title(hints, xwindow);
		return TRUE;
		break;
	case XA_WM_HINTS:
		return TRUE;
		break;
 	case XA_WM_NORMAL_HINTS:
 		get_normal_hints(hints, xwindow);
		return TRUE;
 		break;
	default:
		break;
	}

	if (xevent->xproperty.atom == xa_wm_protocols) {
		get_wm_protocols(hints, xwindow);
		return TRUE;
	}

	return FALSE;
}



static void
get_wm_protocols(GwmHints * hints, Window xwindow)
{
	gboolean take_focus, save_yourself, delete_window;
	Atom *protocols, atype;
	int i, aformat;
	unsigned long nitems, bytes_remain;
	
	protocols = NULL;
	take_focus = FALSE;
	save_yourself = FALSE;
	delete_window = FALSE;

	/* I've seen other window managers try to use the XLib call
	 * for the WM_PROTOCOLS atom, but they all fall back to this
	 * if that doesn't work -- I don't see the point in using the
	 * XLib call at all when this will always work -JMP */

	gdk_error_trap_push();

	if (!XGetWindowProperty(
		GDK_DISPLAY(), xwindow, 
		xa_wm_protocols, 0L, 10L, False, xa_wm_protocols,
		&atype, &aformat, &nitems, &bytes_remain,
		(unsigned char **) &protocols)) {

		gdk_error_trap_pop();
		return;
	}

       gdk_error_trap_pop();

	if (!protocols)
		return;

	for (i = 0; i < nitems; i++) {
		if (protocols[i] == xa_wm_take_focus) {
			take_focus = TRUE;
			continue;
		}

		if (protocols[i] == xa_wm_save_yourself) {
			save_yourself = TRUE;
			continue;
		}

		if (protocols[i] == xa_wm_delete_window) {
			delete_window = TRUE;
			continue;
		}
	}

	XFree((char *)protocols);

	gwm_hints_set_take_focus(hints, take_focus);
	gwm_hints_set_save_yourself(hints, save_yourself);
	gwm_hints_set_delete_window(hints, delete_window);
}



static void
get_title(GwmHints * hints, Window xwindow)
{
	gchar *title;

	gdk_error_trap_push();
	XFetchName(GDK_DISPLAY(), xwindow, &title);
	gdk_error_trap_pop();

	gwm_hints_set_title(hints, title);

	if (title)
		XFree(title);
}


static void
get_icon_title(GwmHints * hints, Window xwindow)
{
	gchar *title;

	gdk_error_trap_push();
	XGetIconName(GDK_DISPLAY(), xwindow, &title);
	gdk_error_trap_pop();

	gwm_hints_set_icon_title(hints, title);

	if (title)
		XFree(title);
}


static void
get_normal_hints(GwmHints * hints, Window xwindow)
{
	gint status;
	long temp;
	XSizeHints xhints;

	gdk_error_trap_push();
 	status = XGetWMNormalHints(GDK_DISPLAY(), xwindow, &xhints, &temp);
	gdk_error_trap_pop();

	if (!status)
		return;

 	if (xhints.flags & PMinSize)
		gwm_hints_set_min_size(
			hints, xhints.min_width, xhints.min_height);

 	if (xhints.flags & PMaxSize)
		gwm_hints_set_max_size(
			hints, xhints.max_width, xhints.max_height);

 	if (xhints.flags & PResizeInc)
		gwm_hints_set_size_incriment(
			hints, xhints.width_inc, xhints.height_inc);

 	if (xhints.flags & PAspect)
		gwm_hints_set_aspect_ratio(
			hints, 
			xhints.min_aspect.x / xhints.min_aspect.y,
			xhints.max_aspect.x / xhints.max_aspect.y);
}
