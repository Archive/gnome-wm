/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GWM_CLIENT_H__
#define __GWM_CLIENT_H__

#include <gtk/gtkwidget.h>
#include "gwmhints.h"

#define GWM_TYPE_CLIENT\
        (gwm_client_get_type())

#define GWM_CLIENT(obj)\
        (GTK_CHECK_CAST((obj), GWM_TYPE_CLIENT, GwmClient))

#define GWM_CLIENT_CLASS(klass)\
        (GTK_CHECK_CLASS_CAST((klass), GWM_TYPE_CLIENT, GwmClientClass))

#define GWM_IS_CLIENT(obj)\
        (GTK_CHECK_TYPE((obj), GWM_TYPE_CLIENT))

#define GWM_IS_CLIENT_CLASS(klass)\
        (GTK_CHECK_CLASS_TYPE((klass), GWM_TYPE_CLIENT))

typedef struct _GwmClient GwmClient;
typedef struct _GwmClientClass GwmClientClass;

struct _GwmClient {
	GtkWidget widget;

	GdkWindow *client_window;
	GwmHints *hints;
};

struct _GwmClientClass {
	GtkWidgetClass parent_class;
};

guint        gwm_client_get_type                  (void);
GwmClient*   gwm_client_new                       (GwmHints * hints);
void         gwm_client_set_hints                 (GwmClient * client,
						   GwmHints * Hints);
gboolean     gwm_client_capture                   (GwmClient * client,
						   guint32 xid);
void         gwm_client_take_focus                (GwmClient * client);
void         gwm_client_close                     (GwmClient * client);

#endif				/* __GWM_CLIENT_H__ */
