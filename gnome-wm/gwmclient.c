/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public Liceinnse for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <X11/Xatom.h>
#include <X11/Xmd.h>
#include <gdk/gdk.h>
#include <gdk/gdkprivate.h>
#include <gdk/gdkx.h>

#include "debug.h"
#include "xatoms.h"
#include "gwmclient.h"

static void gwm_client_class_init             (GwmClientClass * class);
static void gwm_client_init                   (GwmClient * client);
static void gwm_client_finalize               (GtkObject * object);
static void gwm_client_show                   (GtkWidget * widget);
static void gwm_client_hide                   (GtkWidget * widget);
static void gwm_client_map                    (GtkWidget * widget);
static void gwm_client_unmap                  (GtkWidget * widget);
static void gwm_client_realize                (GtkWidget * widget);
static void gwm_client_unrealize              (GtkWidget * widget);
static void gwm_client_size_request           (GtkWidget * widget,
					       GtkRequisition * requisition);
static void gwm_client_size_allocate          (GtkWidget * widget,
				               GtkAllocation * allocation);
static gint gwm_client_enter_notify           (GtkWidget *widget, 
				               GdkEventCrossing *event);
static gint gwm_client_leave_notify           (GtkWidget *widget, 
					       GdkEventCrossing *event);
static GdkFilterReturn w_filter_func          (GdkXEvent * gdk_xevent,
					       GdkEvent * event,
					       gpointer data);
static GdkFilterReturn cw_filter_func         (GdkXEvent * gdk_xevent,
					       GdkEvent * event,
					       gpointer data);

static GtkWidgetClass *parent_class = NULL;

GtkType
gwm_client_get_type(void)
{
	static GtkType client_type = 0;

	if (!client_type) {
		static const GtkTypeInfo client_info = {
			"GwmClient",
			sizeof(GwmClient),
			sizeof(GwmClientClass),
			(GtkClassInitFunc) gwm_client_class_init,
			(GtkObjectInitFunc) gwm_client_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
		
		client_type = gtk_type_unique(gtk_widget_get_type(), 
					      &client_info);
	}

	return client_type;
}


static void
gwm_client_class_init(GwmClientClass * class)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	parent_class = gtk_type_class(gtk_widget_get_type());
	object_class = (GtkObjectClass *) class;
	widget_class = (GtkWidgetClass *) class;

	object_class->finalize = gwm_client_finalize;

	widget_class->show = gwm_client_show;
	widget_class->hide = gwm_client_hide;
	widget_class->map = gwm_client_map;
	widget_class->unmap = gwm_client_unmap;
	widget_class->realize = gwm_client_realize;
	widget_class->unrealize = gwm_client_unrealize;
	widget_class->size_request = gwm_client_size_request;
	widget_class->size_allocate = gwm_client_size_allocate;
	widget_class->enter_notify_event = gwm_client_enter_notify;
	widget_class->leave_notify_event = gwm_client_leave_notify;
}


static void
gwm_client_init(GwmClient * client)
{
	GTK_WIDGET_UNSET_FLAGS(client, GTK_NO_WINDOW);
	client->client_window = NULL;
}


GwmClient *
gwm_client_new(GwmHints * hints)
{
	GwmClient *client;

	client = gtk_type_new(gwm_client_get_type());
	gwm_client_set_hints(client, hints);

	return client;
}


void
gwm_client_set_hints(GwmClient * client, GwmHints * hints)
{
	g_return_if_fail(client != NULL);
	g_return_if_fail(GWM_IS_CLIENT(client));

	if (!hints)
		hints = gwm_hints_new();

	if (client->hints == hints)
		return;

	if (client->hints) {
		gtk_signal_disconnect_by_data(
			GTK_OBJECT(client->hints), client);
		gtk_object_unref(GTK_OBJECT(client->hints));
	}

	client->hints = hints;
	gtk_object_ref(GTK_OBJECT(hints));
	gtk_object_sink(GTK_OBJECT(hints));
}


gboolean
gwm_client_capture(GwmClient * client, guint32 xid)
{
	GtkWidget *widget;
	XWindowAttributes xattrs;

	DEBUG_MESSAGE("gwm_client_capture");

	g_return_val_if_fail(client != NULL, FALSE);
	g_return_val_if_fail(client->client_window == NULL, FALSE);

	widget = GTK_WIDGET(client);

	gdk_error_trap_push();
	XAddToSaveSet(GDK_DISPLAY(), xid);
	gdk_flush();

	if (gdk_window_lookup(xid))
		client->client_window = gdk_window_lookup(xid);
        else
		client->client_window = gdk_window_foreign_new(xid);

	/* maybe the client window is already destroyed */
	if (!client->client_window) {
		gdk_error_trap_pop();
		return FALSE;
	}

	gdk_window_hide(client->client_window);
	gdk_flush();

	/* filter for events on the client window and then select for
	 * the events we want */
	gdk_window_add_filter(client->client_window, cw_filter_func, widget);
	XSelectInput(GDK_DISPLAY(),
		     GDK_WINDOW_XWINDOW(client->client_window),
		     StructureNotifyMask | PropertyChangeMask);
	gdk_flush();


	/* XGetWindowAttributes is used instead of gdk_window_ bla functions
	 * because GDK pretends there is no such thing as border width; 
	 * can't say that I blame GDK for that... */
	XGetWindowAttributes(GDK_DISPLAY(),
			     GDK_WINDOW_XWINDOW(client->client_window),
			     &xattrs);

	gwm_hints_set_position(client->hints, xattrs.x, xattrs.y);
	gwm_hints_set_size(client->hints, xattrs.width, xattrs.height);
	gwm_hints_set_border_width(client->hints, xattrs.border_width);

	if (GTK_WIDGET_REALIZED(widget)) {
		gdk_window_reparent(
			client->client_window, widget->window, 0, 0);
		gdk_flush();
	}

	if (GTK_WIDGET_MAPPED(widget))
		gdk_window_show(client->client_window);

	gdk_error_trap_pop();
	
	gwm_hints_load(client->hints, xid);
	return TRUE;
}


void
gwm_client_take_focus(GwmClient * client)
{
	g_return_if_fail(client != NULL);
	g_return_if_fail(GWM_IS_CLIENT(client));
	g_return_if_fail(client->client_window != NULL);

	DEBUG_MESSAGE("gwm_client_take_focus");
		
	gdk_error_trap_push();
	
	if (client->hints->take_focus) {
		XClientMessageEvent xevent;

		xevent.type = ClientMessage;
		xevent.window = GDK_WINDOW_XWINDOW(client->client_window);
		xevent.message_type = xa_wm_protocols;
		xevent.format = 32;
		xevent.data.l[0] = xa_wm_take_focus;
		xevent.data.l[1] = CurrentTime;


		XSendEvent(GDK_DISPLAY(), 
			   GDK_WINDOW_XWINDOW(client->client_window), 
			   False, 0, (XEvent *) &xevent);
		gdk_flush();
	}

	XSetInputFocus(GDK_DISPLAY(), 
		       GDK_WINDOW_XWINDOW(client->client_window), 
		       RevertToPointerRoot, CurrentTime);

	gdk_flush();
	gdk_error_trap_pop();
}


void
gwm_client_close(GwmClient * client)
{
	g_return_if_fail(client != NULL);
	g_return_if_fail(GWM_IS_CLIENT(client));
	g_return_if_fail(client->client_window != NULL);

	DEBUG_MESSAGE("gwm_clientclose");

	gdk_error_trap_push();
	XKillClient(GDK_DISPLAY(), GDK_WINDOW_XWINDOW(client->client_window));
	gdk_error_trap_pop();
}


static void
gwm_client_set_state(GwmClient * client, gulong state)
{
	gulong data[2];  /* "suggested" by ICCCM version 1 */

	g_return_if_fail(client != NULL);
	g_return_if_fail(GWM_IS_CLIENT(client));

	DEBUG_MESSAGE("gwm_client_set_state");

	data[0] = state;
	data[1] = 0;

	gdk_error_trap_push();
	gdk_flush();
	gdk_property_change(
		client->client_window,
		xa_wm_state, xa_wm_state, 32, GDK_PROP_MODE_REPLACE,
		(unsigned char *) data, 2);
	gdk_flush();
	gdk_error_trap_pop();
}


static void
gwm_client_finalize(GtkObject * object)
{
	GwmClient *client;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GWM_IS_CLIENT(object));

	client = GWM_CLIENT(object);

	if (client->hints)
		gtk_object_unref(GTK_OBJECT(client->hints));

	(* GTK_OBJECT_CLASS(parent_class)->finalize)(object);
}


static void
gwm_client_show(GtkWidget * widget)
{
	GwmClient *client;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_CLIENT(widget));

	client = GWM_CLIENT(widget);

	if (GTK_WIDGET_CLASS(parent_class)->show)
		(*GTK_WIDGET_CLASS(parent_class)->show) (widget);
}


static void
gwm_client_hide(GtkWidget * widget)
{
	GwmClient *client;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_CLIENT(widget));

	client = GWM_CLIENT(widget);

	if (GTK_WIDGET_CLASS(parent_class)->hide)
		(*GTK_WIDGET_CLASS(parent_class)->hide) (widget);
}


static void
gwm_client_map(GtkWidget * widget)
{
	GwmClient *client;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_CLIENT(widget));

	DEBUG_MESSAGE("gwm_client_map");

	client = GWM_CLIENT(widget);

	if (client->client_window) {
		gdk_error_trap_push();
		gdk_window_show(client->client_window);
		gdk_flush();
		gdk_error_trap_pop();
	}

	if (GTK_WIDGET_CLASS(parent_class)->map)
		(*GTK_WIDGET_CLASS(parent_class)->map) (widget);

	gwm_client_set_state(client, NormalState);
}


static void
gwm_client_unmap(GtkWidget * widget)
{
	GwmClient *client;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_CLIENT(widget));

	client = GWM_CLIENT(widget);

	if (client->client_window) {
		gdk_error_trap_push();
		gdk_window_hide(client->client_window);
		gdk_flush();
		gdk_error_trap_pop();
	}

	if (GTK_WIDGET_CLASS(parent_class)->unmap)
		(*GTK_WIDGET_CLASS(parent_class)->unmap) (widget);

	gwm_client_set_state(client, IconicState);
}


static void
gwm_client_realize(GtkWidget * widget)
{
	gint attributes_mask;
	XWindowAttributes xattrs;
	GdkWindowAttr attributes;
	GwmClient *client;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_CLIENT(widget));

	DEBUG_MESSAGE("gwm_client_realize");

	client = GWM_CLIENT(widget);
	GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = widget->allocation.x;
	attributes.y = widget->allocation.y;
	attributes.width = widget->allocation.width;
	attributes.height = widget->allocation.height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual(widget);
	attributes.colormap = gtk_widget_get_colormap(widget);
	attributes.event_mask = gtk_widget_get_events(widget);
	attributes.event_mask |=
	    GDK_EXPOSURE_MASK |
	    GDK_BUTTON_PRESS_MASK |
	    GDK_BUTTON_RELEASE_MASK |
	    GDK_ENTER_NOTIFY_MASK |
	    GDK_LEAVE_NOTIFY_MASK |
	    GDK_KEY_PRESS_MASK |
	    GDK_KEY_RELEASE_MASK;

	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

	widget->window = gdk_window_new(gtk_widget_get_parent_window(widget),
					&attributes,
					attributes_mask);

	gdk_window_set_user_data(widget->window, widget);
	widget->style = gtk_style_attach(widget->style, widget->window);
	gtk_style_set_background(widget->style, widget->window, GTK_STATE_SELECTED);
	gdk_window_add_filter(widget->window, w_filter_func, widget);

	gdk_flush();

	XGetWindowAttributes(GDK_DISPLAY(),
			     GDK_WINDOW_XWINDOW(widget->window),
			     &xattrs);
	
	XSelectInput(GDK_DISPLAY(),
		     GDK_WINDOW_XWINDOW(widget->window),
		     xattrs.your_event_mask | SubstructureRedirectMask);

	gdk_flush();

	/* reparent the client window to the widget's window */
	if (client->client_window) {
		gdk_error_trap_push();
		gdk_window_reparent(
			client->client_window, widget->window, 0, 0);
		gdk_flush();
		gdk_error_trap_pop();
	}
}


static void
gwm_client_unrealize(GtkWidget * widget)
{
	GwmClient *client;

	DEBUG_MESSAGE("gwm_client_unrealize");

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_CLIENT(widget));

	client = GWM_CLIENT(widget);

	if (GTK_WIDGET_CLASS(parent_class)->unrealize)
		(*GTK_WIDGET_CLASS(parent_class)->unrealize) (widget);
}


static void
gwm_client_size_request(GtkWidget * widget, GtkRequisition * requisition)
{
	GwmClient *client;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_CLIENT(widget));

	client = GWM_CLIENT(widget);

	requisition->width = 
		client->hints->width + (2 * client->hints->border_width);
	requisition->height = 
		client->hints->height + (2 * client->hints->border_width);
}


static void
gwm_client_size_allocate(GtkWidget * widget, GtkAllocation * allocation)
{
	gint border_width, width, height;
	GwmClient *client;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_CLIENT(widget));
	g_return_if_fail(allocation != NULL);

	client = GWM_CLIENT(widget);
	widget->allocation = *allocation;

	border_width = client->hints->border_width;
	width = allocation->width - (2 * border_width);
	height = allocation->height - (2 * border_width);

	gwm_hints_set_size(client->hints, width, height);

	if (!GTK_WIDGET_REALIZED(widget))
		return;

	gdk_window_move_resize(
		widget->window, allocation->x, allocation->y, 
		allocation->width, allocation->height);

	if (client->client_window) {
		gint x, y;
		XEvent xevent;

		/* resize the client window */
		gdk_error_trap_push();
		gdk_window_move_resize(
			client->client_window, 0, 0, width, height);
		gdk_flush();

		/* synthasize a ConfigureNotify event so the client knows
		 * what has happened to it; the synthetic ConfigureNotify
		 * events sets the x,y coordinates of the window relative
		 * to the root window */

		gdk_window_get_origin(client->client_window, &x, &y);

		xevent.xconfigure.type = ConfigureNotify;
		xevent.xconfigure.display = GDK_DISPLAY();
		
		xevent.xconfigure.event = 
		  GDK_WINDOW_XWINDOW(client->client_window);
		xevent.xconfigure.window = 
		  GDK_WINDOW_XWINDOW(client->client_window);
		
		xevent.xconfigure.x = x + border_width;
		xevent.xconfigure.y = y + border_width;
		xevent.xconfigure.width = width;
		xevent.xconfigure.height = height;
		
		xevent.xconfigure.border_width = border_width;
		xevent.xconfigure.above = None;
		xevent.xconfigure.override_redirect = False;

		XSendEvent(GDK_DISPLAY(),
			   GDK_WINDOW_XWINDOW(client->client_window),
			   False, StructureNotifyMask, &xevent);
		gdk_flush();
		gdk_error_trap_pop();
	}
}


static gint
gwm_client_enter_notify(GtkWidget *widget, GdkEventCrossing *event)
{
	GwmClient *client;

	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GWM_IS_CLIENT(widget), FALSE);

	client = GWM_CLIENT(widget);

	return TRUE;
}


static gint
gwm_client_leave_notify(GtkWidget *widget, GdkEventCrossing *event)
{
	GwmClient *client;

	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GWM_IS_CLIENT(widget), FALSE);

	client = GWM_CLIENT(widget);

	return TRUE;
}


/*
 * widget->window event filtering
 */

static GdkFilterReturn
w_map_request(GwmClient * client, GdkEvent * event, XEvent * xevent)
{
	if (!GTK_WIDGET_VISIBLE(client))
		gtk_widget_show(GTK_WIDGET(client));

	return GDK_FILTER_REMOVE;
}


static GdkFilterReturn
w_configure_request(GwmClient * client, GdkEvent * event, XEvent * xevent)
{
	XConfigureRequestEvent *xcr;

	xcr = &xevent->xconfigurerequest;

	/* check if position changed */
	if (xcr->value_mask & (CWX | CWY))
		gwm_hints_set_position(client->hints, xcr->x, xcr->y);

	/* check if width/height changed */
	if (xcr->value_mask & (CWWidth | CWHeight))
		gwm_hints_set_size(client->hints, xcr->width, xcr->height);

	/* check if border width change */
	if (xcr->value_mask & CWBorderWidth)
		gwm_hints_set_border_width(client->hints, xcr->border_width);

	gtk_widget_queue_resize(GTK_WIDGET(client));

	return GDK_FILTER_REMOVE;
}


static GdkFilterReturn
w_filter_func(GdkXEvent * gdk_xevent, GdkEvent * event, gpointer data)
{
	GwmClient *client;
	XEvent *xevent;
	GdkFilterReturn return_val;

	g_assert(GWM_IS_CLIENT(data));

	client = GWM_CLIENT(data);
	xevent = (XEvent *) gdk_xevent;

	DEBUG_MESSAGE("w %s: %s", 
		      client->hints->title,
		      debug_xevent_name(xevent->type));

	switch (xevent->type) {
	case MapRequest:
		return_val = w_map_request(client, event, xevent);
		break;
	case ConfigureRequest:
		return_val = w_configure_request(client, event, xevent);
		break;
	default:
		return_val = GDK_FILTER_CONTINUE;
		break;
	}

	return return_val;
}


/* 
 * client->client_window event filtering
 */

static GdkFilterReturn
cw_unmap_notify(GwmClient * client, GdkEvent * event, XEvent * xevent)
{
	if (GTK_WIDGET_VISIBLE(client))
		gtk_widget_hide(GTK_WIDGET(client));

	return GDK_FILTER_REMOVE;
}


static GdkFilterReturn
cw_property_notify(GwmClient * client, GdkEvent * event, XEvent * xevent)
{
	if (gwm_hints_process_property_notify(client->hints, xevent))
		return GDK_FILTER_REMOVE;
	else
		return GDK_FILTER_CONTINUE;
}


static GdkFilterReturn
cw_destroy_notify(GwmClient * client, GdkEvent * event, XEvent * xevent)
{
	gdk_window_destroy_notify(client->client_window);
	client->client_window = NULL;
	gtk_widget_destroy(GTK_WIDGET(client));

	return GDK_FILTER_REMOVE;
}


static GdkFilterReturn
cw_client_message(GwmClient * client, GdkEvent * event, XEvent * xevent)
{
	if (xevent->xclient.message_type == xa_wm_change_state) {
		if (xevent->xclient.data.l[0] != IconicState)
			goto filter_remove;

		if (!GTK_WIDGET_VISIBLE(client))
			goto filter_remove;

		gtk_widget_hide(GTK_WIDGET(client));
		goto filter_remove;
	}

	return GDK_FILTER_CONTINUE;

 filter_remove:
	return GDK_FILTER_REMOVE;
}


static GdkFilterReturn
cw_filter_func(GdkXEvent * gdk_xevent, GdkEvent * event, gpointer data)
{
	GwmClient *client;
	XEvent *xevent;
	GdkFilterReturn return_val;

	g_assert(GWM_IS_CLIENT(data));

	client = GWM_CLIENT(data);
	xevent = (XEvent *) gdk_xevent;

	DEBUG_MESSAGE("cw %s: %s", 
		      client->hints->title, 
		      debug_xevent_name(xevent->type));

	switch (xevent->type) {
	case UnmapNotify:
		return_val = cw_unmap_notify(client, event, xevent);
		break;
	case DestroyNotify:
		return_val = cw_destroy_notify(client, event, xevent);
		break;
	case PropertyNotify:
		return_val = cw_property_notify(client, event, xevent);
		break;
	case ClientMessage:
		return_val = cw_client_message(client, event, xevent);
		break;
	default:
		return_val = GDK_FILTER_CONTINUE;
	}

	return return_val;
}
