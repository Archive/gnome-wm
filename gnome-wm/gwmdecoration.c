/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <string.h>
#include <gtk/gtksignal.h>
#include "debug.h"
#include "gwmmisc.h"
#include "gwmdecoration.h"

static void gwm_decoration_class_init           (GwmDecorationClass * class);
static void gwm_decoration_init                 (GwmDecoration * decoration);
static void gwm_decoration_finalize             (GtkObject * object);

static void gwm_decoration_realize              (GtkWidget * widget);
static void gwm_decoration_unrealize            (GtkWidget * widget);
static void gwm_decoration_map                  (GtkWidget * widget);
static void gwm_decoration_unmap                (GtkWidget * widget);
static void gwm_decoration_size_request         (GtkWidget * widget,
						 GtkRequisition * requisition);
static void gwm_decoration_size_allocate        (GtkWidget * widget,
						 GtkAllocation * allocation);
static void gwm_decoration_draw                 (GtkWidget * widget,
						 GdkRectangle * area);
static gint gwm_decoration_expose               (GtkWidget * widget,
						 GdkEventExpose * event);
static gint gwm_decoration_button_press         (GtkWidget * widget,
						 GdkEventButton * event);
static gint gwm_decoration_button_release       (GtkWidget * widget,
						 GdkEventButton * event);
static gint gwm_decoration_motion               (GtkWidget * widget,
						 GdkEventMotion * event);

static void gwm_decoration_add                  (GtkContainer * container,
						 GtkWidget * widget);

static void real_set_title                      (GwmDecoration * decoration,
						 gchar * title);
static void real_set_position                   (GwmDecoration * decoration,
						 gint x,
						 gint y);
static void real_set_min_size                   (GwmDecoration * decoration,
						 gint min_width,
						 gint min_height);
static void real_set_max_size                   (GwmDecoration * decoration,
						 gint max_width,
						 gint max_height);
static void real_set_size_incriment             (GwmDecoration * decoration,
						 gint width_inc,
						 gint height_inc);
static void real_set_aspect_ratio               (GwmDecoration * decoration,
						 gfloat min_aspect,
						 gfloat max_aspect);
static void real_set_maximized_size             (GwmDecoration * decoration,
						 gint maximized_width,
						 gint maximized_height);
static void real_child_geometry                 (GwmDecoration * decoration, 
						 GwmGeometry * geometry);
static void real_title_geometry                 (GwmDecoration * decoration, 
						 GwmGeometry * geometry);
static void real_close_button_geometry          (GwmDecoration * decoration, 
						 GwmGeometry * geometry);
static void real_iconify_button_geometry        (GwmDecoration * decoration, 
						 GwmGeometry * geometry);
static void real_maximize_button_geometry       (GwmDecoration * decoration, 
						 GwmGeometry * geometry);
static void real_draw_background                (GwmDecoration * decoration,
						 GwmGeometry * geometry,
						 GdkRectangle * area);
static void real_draw_title                     (GwmDecoration * decoration,
						 GwmGeometry * geometry,
						 GdkRectangle * area);
static void real_draw_close_button              (GwmDecoration * decoration,
						 GwmGeometry * geometry,
						 GdkRectangle * area);
static void real_draw_iconify_button            (GwmDecoration * decoration,
						 GwmGeometry * geometry,
						 GdkRectangle * area);
static void real_draw_maximize_button           (GwmDecoration * decoration,
						 GwmGeometry * geometry,
						 GdkRectangle * area);

static void resize_windows_geometry             (GwmDecoration * decoration, 
						 GwmGeometry * geometry);
static void resize_windows_position             (GwmDecoration * decoration);
static void resize_windows_create               (GwmDecoration * decoration);
static void resize_windows_destroy              (GwmDecoration * decoration);
static void resize_windows_map                  (GwmDecoration * decoration);
static void resize_windows_unmap                (GwmDecoration * decoration);
static void close_window_position               (GwmDecoration * decoration);
static void close_window_create                 (GwmDecoration * decoration);
static void close_window_destroy                (GwmDecoration * decoration);
static void close_window_map                    (GwmDecoration * decoration);
static void close_window_unmap                  (GwmDecoration * decoration);
static void iconify_window_position             (GwmDecoration * decoration);
static void iconify_window_create               (GwmDecoration * decoration);
static void iconify_window_destroy              (GwmDecoration * decoration);
static void iconify_window_map                  (GwmDecoration * decoration);
static void iconify_window_unmap                (GwmDecoration * decoration);
static void maximize_window_position            (GwmDecoration * decoration);
static void maximize_window_create              (GwmDecoration * decoration);
static void maximize_window_destroy             (GwmDecoration * decoration);
static void maximize_window_map                 (GwmDecoration * decoration);
static void maximize_window_unmap               (GwmDecoration * decoration);


static void hints_title_changed_cb              (GwmHints * hints,
						 gchar * title,
						 GwmDecoration * decoration);
static void hints_icon_title_changed_cb         (GwmHints * hints,
						 gchar * title,
						 GwmDecoration * decoration);
static void hints_position_changed_cb           (GwmHints * hints,
						 gint x,
						 gint y,
						 GwmDecoration * decoration);
static void hints_min_size_changed_cb           (GwmHints * hints,
						 gint min_width,
						 gint max_height,
						 GwmDecoration * decoration);
static void hints_max_size_changed_cb           (GwmHints * hints,
						 gint max_width,
						 gint max_height,
						 GwmDecoration * decoration);
static void hints_size_incriment_changed_cb     (GwmHints * hints,
						 gint width_inc,
						 gint height_inc,
						 GwmDecoration * decoration);
static void hints_aspect_ratio_changed_cb       (GwmHints * hints,
						 gfloat min_aspect,
						 gfloat max_aspect,
						 GwmDecoration * decoration);

enum {
	SET_TITLE,
	SET_ICON_TITLE,
	SET_POSITION,
	SET_MIN_SIZE,
	SET_MAX_SIZE,
	SET_SIZE_INCRIMENT,
	SET_ASPECT_RATIO,
	SET_MAXIMIZED_SIZE,
	CHILD_GEOMETRY,
	TITLE_GEOMETRY,
	CLOSE_BUTTON_GEOMETRY,
	ICONIFY_BUTTON_GEOMETRY,
	MAXIMIZE_BUTTON_GEOMETRY,
	DRAW_BACKGROUND,
	DRAW_TITLE,
	DRAW_CLOSE_BUTTON,
	DRAW_ICONIFY_BUTTON,
	DRAW_MAXIMIZE_BUTTON,
	CLOSE,
	ICONIFY,
	MAXIMIZE,
	REQUEST_FOCUS,
	LAST_SIGNAL
};

static guint decoration_signals[LAST_SIGNAL] = { 0 };
static GtkBinClass *parent_class = NULL;

GtkType
gwm_decoration_get_type(void)
{
	static GtkType decoration_type = 0;
	
	if (!decoration_type) {
		static const GtkTypeInfo decoration_info = {
			"GwmDecoration",
			sizeof(GwmDecoration),
			sizeof(GwmDecorationClass),
			(GtkClassInitFunc) gwm_decoration_class_init,
			(GtkObjectInitFunc) gwm_decoration_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
		
		decoration_type = gtk_type_unique(gtk_bin_get_type(), 
						  &decoration_info);
	}

	return decoration_type;
}


static void
gwm_decoration_class_init(GwmDecorationClass * class)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;
	GtkContainerClass *container_class;

	object_class = (GtkObjectClass*) class;
	widget_class = (GtkWidgetClass*) class;
	container_class = (GtkContainerClass*) class;
	parent_class = gtk_type_class(gtk_bin_get_type());

	decoration_signals[SET_TITLE] = gtk_signal_new(
		"set_title",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, set_title),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_POINTER);
	
	decoration_signals[SET_ICON_TITLE] = gtk_signal_new(
		"set_icon_title",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, set_icon_title),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_POINTER);

	decoration_signals[SET_POSITION] = gtk_signal_new(
		"set_position",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, set_position),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	decoration_signals[SET_MIN_SIZE] = gtk_signal_new(
		"set_min_size",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, set_min_size),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	decoration_signals[SET_MAX_SIZE] = gtk_signal_new(
		"set_max_size",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, set_max_size),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	decoration_signals[SET_SIZE_INCRIMENT] = gtk_signal_new(
		"set_size_incriment",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, set_size_incriment),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	decoration_signals[SET_ASPECT_RATIO] = gtk_signal_new(
		"set_aspect_ratio",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, set_aspect_ratio),
		gwm_marshal_NONE__FLOAT_FLOAT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_FLOAT, GTK_TYPE_FLOAT);

	decoration_signals[SET_MAXIMIZED_SIZE] = gtk_signal_new(
		"set_maximized_size",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, set_maximized_size),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	decoration_signals[CHILD_GEOMETRY] = gtk_signal_new(
		"child_geometry",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, child_geometry),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1, 
		GTK_TYPE_POINTER);

	decoration_signals[TITLE_GEOMETRY] = gtk_signal_new(
		"title_geometry",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, title_geometry),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1, 
		GTK_TYPE_POINTER);

	decoration_signals[CLOSE_BUTTON_GEOMETRY] = gtk_signal_new(
		"close_button_geometry",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, close_button_geometry),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1, 
		GTK_TYPE_POINTER);

	decoration_signals[ICONIFY_BUTTON_GEOMETRY] = gtk_signal_new(
		"iconify_button_geometry",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, iconify_button_geometry),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1, 
		GTK_TYPE_POINTER);

	decoration_signals[MAXIMIZE_BUTTON_GEOMETRY] = gtk_signal_new(
		"maximize_button_geometry",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass,maximize_button_geometry),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1, 
		GTK_TYPE_POINTER);

	decoration_signals[DRAW_BACKGROUND] = gtk_signal_new(
		"draw_background",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, draw_background),
		gtk_marshal_NONE__POINTER_POINTER,
		GTK_TYPE_NONE, 2, 
		GTK_TYPE_POINTER, GTK_TYPE_POINTER);

	decoration_signals[DRAW_TITLE] = gtk_signal_new(
		"draw_title",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, draw_title),
		gtk_marshal_NONE__POINTER_POINTER,
		GTK_TYPE_NONE, 2, 
		GTK_TYPE_POINTER, GTK_TYPE_POINTER);

	decoration_signals[DRAW_CLOSE_BUTTON] = gtk_signal_new(
		"draw_close_button",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, draw_close_button),
		gtk_marshal_NONE__POINTER_POINTER,
		GTK_TYPE_NONE, 2, 
		GTK_TYPE_POINTER, GTK_TYPE_POINTER);

	decoration_signals[DRAW_ICONIFY_BUTTON] = gtk_signal_new(
		"draw_iconify_button",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, draw_iconify_button),
		gtk_marshal_NONE__POINTER_POINTER,
		GTK_TYPE_NONE, 2, 
		GTK_TYPE_POINTER, GTK_TYPE_POINTER);

	decoration_signals[DRAW_MAXIMIZE_BUTTON] = gtk_signal_new(
		"draw_maximize_button",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, draw_maximize_button),
		gtk_marshal_NONE__POINTER_POINTER,
		GTK_TYPE_NONE, 2, 
		GTK_TYPE_POINTER, GTK_TYPE_POINTER);

	decoration_signals[CLOSE] = gtk_signal_new(
		"close",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, close),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	decoration_signals[ICONIFY] = gtk_signal_new(
		"iconify",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, iconify),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	decoration_signals[MAXIMIZE] = gtk_signal_new(
		"maximize",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, maximize),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	decoration_signals[REQUEST_FOCUS] = gtk_signal_new(
		"request_focus",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmDecorationClass, request_focus),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals(
		object_class, decoration_signals, LAST_SIGNAL);

	object_class->finalize = gwm_decoration_finalize;

	widget_class->realize = gwm_decoration_realize;
	widget_class->unrealize = gwm_decoration_unrealize;
	widget_class->map = gwm_decoration_map;
	widget_class->unmap = gwm_decoration_unmap;
	widget_class->size_request = gwm_decoration_size_request;
	widget_class->size_allocate = gwm_decoration_size_allocate;
	widget_class->draw = gwm_decoration_draw;
	widget_class->expose_event = gwm_decoration_expose;
	widget_class->button_press_event = gwm_decoration_button_press;
	widget_class->button_release_event = gwm_decoration_button_release;
	widget_class->motion_notify_event = gwm_decoration_motion;

	container_class->add = gwm_decoration_add;

	class->set_title = real_set_title;
	class->set_position = real_set_position;
	class->set_min_size = real_set_min_size;
	class->set_max_size = real_set_max_size;
	class->set_size_incriment = real_set_size_incriment;
	class->set_aspect_ratio = real_set_aspect_ratio;
	class->set_maximized_size = real_set_maximized_size;
	class->child_geometry = real_child_geometry;
	class->title_geometry = real_title_geometry;
	class->close_button_geometry = real_close_button_geometry;
	class->iconify_button_geometry = real_iconify_button_geometry;
	class->maximize_button_geometry = real_maximize_button_geometry;
	class->draw_background = real_draw_background;
	class->draw_title = real_draw_title;
	class->draw_close_button = real_draw_close_button;
	class->draw_iconify_button = real_draw_iconify_button;
	class->draw_maximize_button = real_draw_maximize_button;
	class->close = NULL;
	class->iconify = NULL;
	class->maximize = NULL;
	class->request_focus = NULL;
}


static void
gwm_decoration_init(GwmDecoration * decoration)
{
	DEBUG_MESSAGE("gwm_decoration_init");

	GTK_WIDGET_UNSET_FLAGS(decoration, GTK_NO_WINDOW);
	gtk_container_set_resize_mode(
		GTK_CONTAINER(decoration), GTK_RESIZE_QUEUE);

	decoration->hints = NULL;
	decoration->state = GWM_DECORATION_STATE_NORMAL;
	decoration->state_data = NULL;
	
	memset(&decoration->windows, 0, sizeof(GwmDecorationWindows));
	memset(&decoration->cursors, 0, sizeof(GwmDecorationCursors));

	decoration->resize_border_width = 4;
	decoration->corner_resize_length = 15;
	decoration->titlebar_height = 25;
}


static void
gwm_decoration_finalize(GtkObject * object)
{
	GwmDecoration *decoration;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GWM_IS_DECORATION(object));

	decoration = GWM_DECORATION(object);

	if (decoration->hints)
		gtk_object_unref(GTK_OBJECT(decoration->hints));

	(* GTK_OBJECT_CLASS(parent_class)->finalize)(object);
}

/**** GtkWidget Methods ****/

static void
gwm_decoration_realize(GtkWidget * widget)
{
	gint attributes_mask;
	GdkWindowAttr attributes;
	GwmDecoration *decoration;

	DEBUG_MESSAGE("gwm_decoration_realize");

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_DECORATION(widget));

	decoration = GWM_DECORATION(widget);
	GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = widget->allocation.x;
	attributes.y = widget->allocation.y;
	attributes.width = widget->allocation.width;
	attributes.height = widget->allocation.height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual(widget);
	attributes.colormap = gtk_widget_get_colormap(widget);

	attributes.event_mask = gtk_widget_get_events(widget);
	attributes.event_mask |= 
		GDK_EXPOSURE_MASK|GDK_BUTTON_PRESS_MASK| 
		GDK_BUTTON_RELEASE_MASK|GDK_ENTER_NOTIFY_MASK| 
		GDK_LEAVE_NOTIFY_MASK|GDK_KEY_PRESS_MASK| 
		GDK_KEY_RELEASE_MASK;

	attributes_mask = 
		GDK_WA_X|GDK_WA_Y|GDK_WA_VISUAL|GDK_WA_COLORMAP;

	widget->window = gdk_window_new(
		gtk_widget_get_parent_window(widget),
		&attributes, 
		attributes_mask);
	gdk_window_set_user_data(widget->window, widget);
	widget->style = gtk_style_attach(widget->style, widget->window);
	gtk_style_set_background(
		widget->style, widget->window, GTK_STATE_NORMAL);

	if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
		/* now that the style has been attached, we can compute the
		 * titlebar height */
		decoration->titlebar_height = 
			widget->style->font->ascent +
			widget->style->font->descent +
			decoration->resize_border_width;

		resize_windows_create(decoration);
		close_window_create(decoration);
		iconify_window_create(decoration);

		if (GWM_HINTS_HAS_MAXIMIZE(decoration->hints))
			maximize_window_create(decoration);
	}
}


static void
gwm_decoration_unrealize(GtkWidget * widget)
{
	GwmDecoration *decoration;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_DECORATION(widget));

	decoration = GWM_DECORATION(widget);

	if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
		/* unset setting selected by styles */
		decoration->titlebar_height = 0;

		resize_windows_destroy(decoration);
		close_window_destroy(decoration);
		iconify_window_destroy(decoration);

		if (GWM_HINTS_HAS_MAXIMIZE(decoration->hints))
			maximize_window_destroy(decoration);
	}

	if (GTK_WIDGET_CLASS(parent_class)->unrealize)
		(*GTK_WIDGET_CLASS(parent_class)->unrealize)(widget);
}


static void
gwm_decoration_map(GtkWidget * widget)
{
	GwmDecoration *decoration;

	DEBUG_MESSAGE("gwm_decoration_map");

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_DECORATION(widget));

	decoration = GWM_DECORATION(widget);

	if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
		resize_windows_map(decoration);
		close_window_map(decoration);
		iconify_window_map(decoration);
		
		if (GWM_HINTS_HAS_MAXIMIZE(decoration->hints))
			maximize_window_map(decoration);
	}

	if (GTK_WIDGET_CLASS(parent_class)->map)
		(*GTK_WIDGET_CLASS(parent_class)->map)(widget);
}


static void
gwm_decoration_unmap(GtkWidget * widget)
{
	GwmDecoration *decoration;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_DECORATION(widget));

	decoration = GWM_DECORATION(widget);

	if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
		resize_windows_unmap(decoration);
		close_window_unmap(decoration);
		iconify_window_unmap(decoration);

		if (GWM_HINTS_HAS_MAXIMIZE(decoration->hints))
			maximize_window_unmap(decoration);
	}

	if (GTK_WIDGET_CLASS(parent_class)->unmap)
		(*GTK_WIDGET_CLASS(parent_class)->unmap)(widget);
}


static void
gwm_decoration_size_request(GtkWidget * widget, GtkRequisition * requisition)
{
	GtkBin *bin;
	GwmDecoration *decoration;

	DEBUG_MESSAGE("gwm_decoration_size_request");

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_DECORATION(widget));
	g_return_if_fail(requisition != NULL);

	bin = GTK_BIN(widget);
	decoration = GWM_DECORATION(widget);

	requisition->width = 0;
	requisition->height = 0;

	if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
		requisition->width += (decoration->resize_border_width * 2);
		requisition->height += (decoration->resize_border_width * 2);

		if (GWM_HINTS_HAS_TITLE(decoration->hints))
			requisition->height += decoration->titlebar_height;
	}

	if (bin->child) {
		GtkRequisition child_requisition;
		
		gtk_widget_size_request(bin->child, &child_requisition);
		requisition->width += child_requisition.width;
		requisition->height += child_requisition.height;
	}

	DEBUG_MESSAGE("gwm_decoration_size_request width=%d height=%d",
		      requisition->width,
		      requisition->height);
}


static void
gwm_decoration_size_allocate(GtkWidget * widget, GtkAllocation * allocation)
{
	GtkBin *bin;
	GwmDecoration *decoration;

	DEBUG_MESSAGE("gwm_decoration_size_allocate x=%d y=%d w=%d h=%d",
		      allocation->x,
		      allocation->y,
		      allocation->width,
		      allocation->height);

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_DECORATION(widget));
	g_return_if_fail(allocation != NULL);

	bin = GTK_BIN(widget);
	decoration = GWM_DECORATION(widget);
	widget->allocation = *allocation;

	if (GTK_WIDGET_REALIZED(widget)) {
		gdk_window_move_resize(
			widget->window,
			allocation->x, allocation->y,
			allocation->width, allocation->height);

		if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
			resize_windows_position(decoration);
			close_window_position(decoration);
			iconify_window_position(decoration);

			if (GWM_HINTS_HAS_MAXIMIZE(decoration->hints))
				maximize_window_position(decoration);
		}
	}

	/* this allocation is important even if the GwmDecoration only
	 * moved, because the GwmClient widget needs to send a synthetic
	 * ConfigureNotify event to the client application! -JMP */

	if (bin->child && GTK_WIDGET_VISIBLE(bin->child)) {
		GwmGeometry geometry;
		GtkAllocation child_allocation;

		gwm_decoration_child_geometry(decoration, &geometry);

		child_allocation.x = geometry.x;
		child_allocation.y = geometry.y;
		child_allocation.width = geometry.width;
		child_allocation.height = geometry.height;
		gtk_widget_size_allocate(bin->child, &child_allocation);
	}
}

static void
gwm_decoration_draw(GtkWidget * widget, GdkRectangle * area)
{
	GwmDecoration *decoration;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_DECORATION(widget));

	decoration = GWM_DECORATION(widget);

	if (!GTK_WIDGET_DRAWABLE(widget))
		return;

	gdk_window_clear_area(
		widget->window, area->x, area->y, area->width, area->height);

	if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
		gwm_decoration_draw_background(decoration, area);
		gwm_decoration_draw_close_button(decoration, area);
		gwm_decoration_draw_iconify_button(decoration, area);
		gwm_decoration_draw_maximize_button(decoration, area);

		if (GWM_HINTS_HAS_TITLE(decoration->hints))
			gwm_decoration_draw_title(decoration, area);
	}

	if (GTK_WIDGET_CLASS(parent_class)->draw)
		(*GTK_WIDGET_CLASS(parent_class)->draw)(widget, area);
}


static gint
gwm_decoration_expose(GtkWidget * widget, GdkEventExpose * event)
{
	GwmDecoration *decoration;

	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GWM_IS_DECORATION(widget), FALSE);
	g_return_val_if_fail(event != NULL, FALSE);

	decoration = GWM_DECORATION(widget);

	if (!GTK_WIDGET_DRAWABLE(widget)) 
		return FALSE;

	if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
		gwm_decoration_draw_background(decoration, &event->area);
		gwm_decoration_draw_close_button(decoration, &event->area);
		gwm_decoration_draw_iconify_button(decoration, &event->area);
		gwm_decoration_draw_maximize_button(decoration, &event->area);

		if (GWM_HINTS_HAS_TITLE(decoration->hints))
			gwm_decoration_draw_title(decoration, &event->area);
	}

	if (GTK_WIDGET_CLASS(parent_class)->expose_event)
		(*GTK_WIDGET_CLASS(parent_class)->expose_event)(widget, event);

	return FALSE;
}


static gboolean
move_begin(GwmDecoration * decoration, GdkEventButton * event)
{
	gint x1, y1, x2, y2;
	GwmDecorationMoveInfo *minfo;

	if (GTK_WIDGET(decoration)->window == event->window)
		goto do_move;
	
	return FALSE;

 do_move:
	
	if (gdk_pointer_grab(
		GTK_WIDGET(decoration)->window, FALSE,
		GDK_POINTER_MOTION_MASK|GDK_BUTTON_RELEASE_MASK,
		NULL, NULL, event->time))
		return FALSE;

	decoration->state = GWM_DECORATION_STATE_MOVE;
	decoration->state_data = minfo = g_new(GwmDecorationMoveInfo, 1);

	gdk_window_raise(GTK_WIDGET(decoration)->window);

	gtk_grab_add(GTK_WIDGET(decoration));
	gdk_window_get_position(GTK_WIDGET(decoration)->window, &x1, &y1);
	gdk_window_get_pointer(NULL, &x2, &y2, NULL);
	minfo->xoffset = x2 - x1;
	minfo->yoffset = y2 - y1;

	return TRUE;
}


static void
move_motion(GwmDecoration * decoration)
{
	gint x, y;
	GwmDecorationMoveInfo *minfo;

	minfo = (GwmDecorationMoveInfo *) decoration->state_data;

	gdk_window_get_pointer(NULL, &x, &y, NULL);
	x = x - minfo->xoffset;
	y = y - minfo->yoffset;

	gwm_hints_set_position(GWM_HINTS(decoration->hints), x, y);
}


static void
move_end(GwmDecoration * decoration, GdkEventButton * event)
{ 
	decoration->state = GWM_DECORATION_STATE_NORMAL;
	g_free(decoration->state_data);
	decoration->state_data = NULL;

	gtk_grab_remove(GTK_WIDGET(decoration));
	gdk_pointer_ungrab(event->time);
}


static gboolean
resize_begin(GwmDecoration * decoration, GdkEventButton * event)
{
	gint x, y;
	GdkCursor *cursor;
	GwmDecorationWindows *ws;
	GwmDecorationResizeState state;
	GwmDecorationResizeInfo *rinfo;

	ws = &decoration->windows;

	if (event->window == ws->tl1w || event->window == ws->tl2w) {
		state = GWM_DECORATION_RESIZE_TOP_LEFT;
		cursor = decoration->cursors.tlc;
		goto do_resize;
	}
	if (event->window == ws->tw) {
		state = GWM_DECORATION_RESIZE_TOP;
		cursor = decoration->cursors.tc;
		goto do_resize;
	}
	if (event->window == ws->tr1w || event->window ==  ws->tr2w) {
		state = GWM_DECORATION_RESIZE_TOP_RIGHT;
		cursor = decoration->cursors.trc;
		goto do_resize;
	}
	if (event->window == ws->rw) {
		state = GWM_DECORATION_RESIZE_RIGHT;
		cursor = decoration->cursors.rc;
		goto do_resize;
	}
	if (event->window == ws->br1w || event->window ==  ws->br2w) {
		state = GWM_DECORATION_RESIZE_BOTTOM_RIGHT;
		cursor = decoration->cursors.brc;
		goto do_resize;
	}
	if (event->window == ws->bw) {
		state = GWM_DECORATION_RESIZE_BOTTOM;
		cursor = decoration->cursors.bc;
		goto do_resize;
	}
	if (event->window == ws->bl1w || event->window ==  ws->bl2w) {
		state = GWM_DECORATION_RESIZE_BOTTOM_LEFT;
		cursor = decoration->cursors.blc;
		goto do_resize;
	}
	if (event->window == ws->lw) {
		state = GWM_DECORATION_RESIZE_LEFT;
		cursor = decoration->cursors.lc;
		goto do_resize;
	}

	return FALSE;

 do_resize:

	if (gdk_pointer_grab(
		GTK_WIDGET(decoration)->window, FALSE,
		GDK_POINTER_MOTION_MASK|GDK_BUTTON_RELEASE_MASK,
		NULL, cursor, event->time))
		return FALSE;

	decoration->state = GWM_DECORATION_STATE_RESIZE;
	decoration->state_data = rinfo = g_new(GwmDecorationResizeInfo, 1);
	rinfo->state = state;

	gdk_window_raise(GTK_WIDGET(decoration)->window);

	gtk_grab_add(GTK_WIDGET(decoration));
	gdk_window_get_pointer(NULL, &x, &y, NULL);
	rinfo->xoffset = x;
	rinfo->yoffset = y;
	rinfo->orig_x = GTK_WIDGET(decoration)->allocation.x;
	rinfo->orig_y = GTK_WIDGET(decoration)->allocation.y;
	rinfo->orig_width = GTK_WIDGET(decoration)->allocation.width;
	rinfo->orig_height = GTK_WIDGET(decoration)->allocation.height;

	return TRUE;
}


static void
resize_motion(GwmDecoration * decoration)
{
	gint x, y, width, height, delta_x, delta_y, delta_width, delta_height;
	GwmDecorationResizeInfo *rinfo;

	rinfo = (GwmDecorationResizeInfo *) decoration->state_data;

	/* first, figure out the size change */
	gdk_window_get_pointer(NULL, &x, &y, NULL);
	delta_x = x - rinfo->xoffset;
	delta_y = y - rinfo->yoffset;

	switch (rinfo->state) {
	case GWM_DECORATION_RESIZE_TOP_LEFT:
		delta_width = -delta_x;
		delta_height = -delta_y;
		delta_x = delta_x;
		delta_y = delta_y;
		break;

	case GWM_DECORATION_RESIZE_TOP:
		delta_width = 0;
		delta_height = -delta_y;
		delta_x = 0;
		delta_y = delta_y;
		break;

	case GWM_DECORATION_RESIZE_TOP_RIGHT:
		delta_width = delta_x;
		delta_height = -delta_y;
		delta_x = 0;
		delta_y = delta_y;
		break;

	case GWM_DECORATION_RESIZE_RIGHT:
		delta_width = delta_x;
		delta_height = 0;
		delta_x = 0;
		delta_y = 0;
		break;

	case GWM_DECORATION_RESIZE_BOTTOM_RIGHT:
		delta_width = delta_x;
		delta_height = delta_y;
		delta_x = 0;
		delta_y = 0;
		break;

	case GWM_DECORATION_RESIZE_BOTTOM:
		delta_width = 0;
		delta_height = delta_y;
		delta_x = 0;
		delta_y = 0;
		break;

	case GWM_DECORATION_RESIZE_BOTTOM_LEFT:
		delta_width = -delta_x;
		delta_height = delta_y;
		delta_x = delta_x;
		delta_y = 0;
		break;

	case GWM_DECORATION_RESIZE_LEFT:
		delta_width = -delta_x;
		delta_height = 0;
		delta_x = delta_x;
		delta_y = 0;
		break;
	}

	if (delta_x || delta_y) {
		x = rinfo->orig_x + delta_x;
		y = rinfo->orig_y + delta_y;
	} else {
		x = GTK_WIDGET(decoration)->allocation.x;
		y = GTK_WIDGET(decoration)->allocation.y;
	}

	/* apply size incriment boundaries to the deltas */
	delta_width = 
	  delta_width - (delta_width % decoration->hints->width_inc);
	delta_height =
	  delta_height - (delta_height % decoration->hints->height_inc);

	width = rinfo->orig_width + delta_width;
	height = rinfo->orig_height + delta_height;

	if (width < 0 || height < 0) {
		DEBUG_MESSAGE("eeeekkkk!");
		return;
	}

	/* clamp width and height to min/max window size */
	width = CLAMP(
		decoration->hints->min_width, 
		width,
		decoration->hints->max_width);

	height = CLAMP(
		decoration->hints->min_height, 
		height, 
		decoration->hints->max_height);

	/* re-use delta_width and delta_height */
	delta_width = 0;
	delta_height = 0;

	if (delta_width || delta_height) {
		switch (rinfo->state) {
		case GWM_DECORATION_RESIZE_TOP_LEFT:
			width = width + delta_width;
			height = height + delta_height;
			x = x - delta_width;
			y = y - delta_height;
			break;
			
		case GWM_DECORATION_RESIZE_TOP:
			height = height + delta_height;
			y = y - delta_height;
			break;
			
		case GWM_DECORATION_RESIZE_TOP_RIGHT:
			width = width + delta_width;
			height = height + delta_height;
			y = y - delta_height;
			break;
			
		case GWM_DECORATION_RESIZE_RIGHT:
			width = width + delta_width;
			break;
			
		case GWM_DECORATION_RESIZE_BOTTOM_RIGHT:
			width = width + delta_width;
			height = height + delta_height;
			break;
			
		case GWM_DECORATION_RESIZE_BOTTOM:
			height = height + delta_height;
			break;
			
		case GWM_DECORATION_RESIZE_BOTTOM_LEFT:
			width = width + delta_width;
			height = height + delta_height;
			x = x - delta_width;
			break;
			
		case GWM_DECORATION_RESIZE_LEFT:
			width = width + delta_width;
			x = x - delta_width;
			break;
		}
	}

	if (x != GTK_WIDGET(decoration)->allocation.x ||
	    y != GTK_WIDGET(decoration)->allocation.y ||
	    width != GTK_WIDGET(decoration)->allocation.width ||
	    height != GTK_WIDGET(decoration)->allocation.height) {
		GtkAllocation allocation;

		allocation.x = x;
		allocation.y = y;
		allocation.width = width;
		allocation.height = height;

		gwm_decoration_size_allocate(
			GTK_WIDGET(decoration), &allocation);
	}
}


static void
resize_end(GwmDecoration * decoration, GdkEventButton * event)
{
	decoration->state = GWM_DECORATION_STATE_NORMAL;
	g_free(decoration->state_data);
	decoration->state_data = NULL;

	gtk_grab_remove(GTK_WIDGET(decoration));
	gdk_pointer_ungrab(event->time);
}


static gint
gwm_decoration_button_press(GtkWidget * widget, GdkEventButton * event)
{
	GwmDecoration *decoration;

	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GWM_IS_DECORATION(widget), FALSE);
	g_return_val_if_fail(event != NULL, FALSE);

	decoration = GWM_DECORATION(widget);

	if (GWM_HINTS_HAS_BORDER(decoration->hints)) {
		if (move_begin(decoration, event))
			goto done;

		if (resize_begin(decoration, event))
			goto done;
	}

 done:
	return FALSE;
}


static gint
gwm_decoration_button_release(GtkWidget * widget, GdkEventButton * event)
{
	GwmDecoration *decoration;

	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GWM_IS_DECORATION(widget), FALSE);
	g_return_val_if_fail(event != NULL, FALSE);

	decoration = GWM_DECORATION(widget);

	switch (decoration->state) {
	case GWM_DECORATION_STATE_NORMAL:
		if (event->window == decoration->windows.cw) {
			gwm_decoration_close(decoration);
			goto done;
		}
		if (event->window == decoration->windows.iw) {
			gwm_decoration_iconify(decoration);
			goto done;
		}
		if (event->window == decoration->windows.mw) {
			gwm_decoration_maximize(decoration);
			goto done;
		}
		break;
	case GWM_DECORATION_STATE_MOVE:
		move_end(decoration, event);
		break;
	case GWM_DECORATION_STATE_RESIZE:
		resize_end(decoration, event);
		break;

	}

 done:
	return FALSE;
}


static gint
gwm_decoration_motion(GtkWidget * widget, GdkEventMotion * event)
{
	GwmDecoration *decoration;

	g_return_val_if_fail(widget != NULL, FALSE);
	g_return_val_if_fail(GWM_IS_DECORATION(widget), FALSE);
	g_return_val_if_fail(event != NULL, FALSE);


	decoration = GWM_DECORATION(widget);

	switch (decoration->state) {
	case GWM_DECORATION_STATE_NORMAL:
		break;
	case GWM_DECORATION_STATE_MOVE:
		move_motion(decoration);
		break;
	case GWM_DECORATION_STATE_RESIZE:
		resize_motion(decoration);
		break;
	}

	return FALSE;
}

/**** GtkContainer Methods ****/

static void
gwm_decoration_add(GtkContainer * container, GtkWidget * widget)
{
	if (GTK_CONTAINER_CLASS(parent_class)->add)
		(*GTK_CONTAINER_CLASS(parent_class)->add)(container, widget);
}

/**** GwmDecoration Methods ****/

GwmDecoration *
gwm_decoration_new(GwmHints * hints)
{
	GwmDecoration *decoration;

	decoration = gtk_type_new(gwm_decoration_get_type());
	gwm_decoration_set_hints(decoration, hints);

	return decoration;
}


void
gwm_decoration_set_hints(GwmDecoration * decoration, GwmHints * hints)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	if (!hints)
		hints = gwm_hints_new();

	if (decoration->hints == hints)
		return;

	if (decoration->hints) {
		gtk_signal_disconnect_by_data(
			GTK_OBJECT(decoration->hints), decoration);
		gtk_object_unref(GTK_OBJECT(decoration->hints));
	}

	decoration->hints = hints;
	gtk_object_ref(GTK_OBJECT(hints));
	gtk_object_sink(GTK_OBJECT(hints));

	gtk_signal_connect(GTK_OBJECT(hints),
			   "title_changed",
			   hints_title_changed_cb,
			   decoration);

	gtk_signal_connect(GTK_OBJECT(hints),
			   "icon_title_changed",
			   hints_icon_title_changed_cb,
			   decoration);

	gtk_signal_connect(GTK_OBJECT(hints),
			   "position_changed",
			   hints_position_changed_cb,
			   decoration);

	gtk_signal_connect(GTK_OBJECT(hints),
			   "min_size_changed",
			   hints_min_size_changed_cb,
			   decoration);

	gtk_signal_connect(GTK_OBJECT(hints),
			   "max_size_changed",
			   hints_max_size_changed_cb,
			   decoration);

	gtk_signal_connect(GTK_OBJECT(hints),
			   "size_incriment_changed",
			   hints_size_incriment_changed_cb,
			   decoration);

	gtk_signal_connect(GTK_OBJECT(hints),
			   "aspect_ratio_changed",
			   hints_aspect_ratio_changed_cb,
			   decoration);

	gwm_decoration_set_title(decoration, hints->title);
	gwm_decoration_set_icon_title(decoration, hints->icon_title);
	gwm_decoration_set_position(decoration, hints->x, hints->y);
	gwm_decoration_set_min_size(
		decoration, hints->min_width, hints->min_height);
	gwm_decoration_set_max_size(
		decoration, hints->max_width, hints->max_height);
	gwm_decoration_set_size_incriment(
		decoration, hints->width_inc, hints->height_inc);
	gwm_decoration_set_aspect_ratio(
		decoration, hints->min_aspect, hints->max_aspect);
}


void
gwm_decoration_set_title(GwmDecoration * decoration, gchar * title)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[SET_TITLE], 
			title);
}


void
gwm_decoration_set_icon_title(GwmDecoration * decoration, gchar * title)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[SET_ICON_TITLE], 
			title);
}


void
gwm_decoration_set_position(GwmDecoration * decoration, gint x, gint y)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[SET_POSITION], 
			x, y);
}


void
gwm_decoration_set_min_size(GwmDecoration * decoration, 
			    gint min_width, gint min_height)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[SET_MIN_SIZE], 
			min_width, min_height);
}


void
gwm_decoration_set_max_size(GwmDecoration * decoration, 
			    gint max_width, gint max_height)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[SET_MAX_SIZE], 
			max_width, max_height);
}


void
gwm_decoration_set_size_incriment(GwmDecoration * decoration,
				  gint width_inc, gint height_inc)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[SET_SIZE_INCRIMENT], 
			width_inc, height_inc);
}


void
gwm_decoration_set_aspect_ratio(GwmDecoration * decoration,
				gfloat min_aspect, gfloat max_aspect)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[SET_ASPECT_RATIO], 
			min_aspect, max_aspect);
}


void
gwm_decoration_set_maximized_size(GwmDecoration * decoration,
				  gint maximized_width, gint maximized_height)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[SET_MAXIMIZED_SIZE], 
			maximized_width, maximized_height);
}


void
gwm_decoration_child_geometry(GwmDecoration * decoration,
			      GwmGeometry * geometry)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));
	g_return_if_fail(geometry != NULL);

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[CHILD_GEOMETRY],
			geometry);
}


void
gwm_decoration_title_geometry(GwmDecoration * decoration,
			      GwmGeometry * geometry)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));
	g_return_if_fail(geometry != NULL);

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[TITLE_GEOMETRY],
			geometry);
}


void
gwm_decoration_close_button_geometry(GwmDecoration * decoration,
				     GwmGeometry * geometry)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));
	g_return_if_fail(geometry != NULL);

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[CLOSE_BUTTON_GEOMETRY],
			geometry);
}


void
gwm_decoration_iconify_button_geometry(GwmDecoration * decoration,
				       GwmGeometry * geometry)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));
	g_return_if_fail(geometry != NULL);

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[ICONIFY_BUTTON_GEOMETRY],
			geometry);
}


void
gwm_decoration_maximize_button_geometry(GwmDecoration * decoration,
					GwmGeometry * geometry)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));
	g_return_if_fail(geometry != NULL);

	gtk_signal_emit(GTK_OBJECT(decoration),
			decoration_signals[MAXIMIZE_BUTTON_GEOMETRY],
			geometry);
}


void
gwm_decoration_draw_background(GwmDecoration * decoration,
			       GdkRectangle * area)
{
	GwmGeometry geometry;

	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[DRAW_BACKGROUND],
			geometry, area);
}


void
gwm_decoration_draw_title(GwmDecoration * decoration,
			  GdkRectangle * area)
{
	GwmGeometry geometry;

	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gwm_decoration_title_geometry(decoration, &geometry);
	if (!gwm_geometry_intersects_area(&geometry, area))
		return;

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[DRAW_TITLE],
			&geometry, area);
}


void
gwm_decoration_draw_close_button(GwmDecoration * decoration, 
				 GdkRectangle * area)
{
	GwmGeometry geometry;

	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gwm_decoration_close_button_geometry(decoration, &geometry);
	if (!gwm_geometry_intersects_area(&geometry, area))
		return;

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[DRAW_CLOSE_BUTTON],
			&geometry, area);
}


void
gwm_decoration_draw_iconify_button(GwmDecoration * decoration, 
				   GdkRectangle * area)
{
	GwmGeometry geometry;

	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gwm_decoration_iconify_button_geometry(decoration, &geometry);
	if (!gwm_geometry_intersects_area(&geometry, area))
		return;

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[DRAW_ICONIFY_BUTTON],
			&geometry, area);
}


void
gwm_decoration_draw_maximize_button(GwmDecoration * decoration, 
				    GdkRectangle * area)
{
	GwmGeometry geometry;

	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gwm_decoration_maximize_button_geometry(decoration, &geometry);
	if (!gwm_geometry_intersects_area(&geometry, area))
		return;

	gtk_signal_emit(GTK_OBJECT(decoration), 
			decoration_signals[DRAW_MAXIMIZE_BUTTON],
			&geometry, area);
}


void
gwm_decoration_close(GwmDecoration * decoration)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration), decoration_signals[CLOSE]);
}


void
gwm_decoration_iconify(GwmDecoration * decoration)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration), decoration_signals[ICONIFY]);
}


void
gwm_decoration_maximize(GwmDecoration * decoration)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(GTK_OBJECT(decoration), decoration_signals[MAXIMIZE]);
}


void
gwm_decoration_request_focus(GwmDecoration * decoration)
{
	g_return_if_fail(decoration != NULL);
	g_return_if_fail(GWM_IS_DECORATION(decoration));

	gtk_signal_emit(
		GTK_OBJECT(decoration), decoration_signals[REQUEST_FOCUS]);

}

/**** real implimentations of GwmDecoration methods ****/

static void
real_set_title(GwmDecoration * decoration, gchar * title)
{
	if (GTK_WIDGET_DRAWABLE(decoration))
		gtk_widget_queue_draw(GTK_WIDGET(decoration));
}


static void
real_set_position(GwmDecoration * decoration, gint x, gint y)
{
	GTK_WIDGET(decoration)->allocation.x = x;
	GTK_WIDGET(decoration)->allocation.y = y;

	if (GTK_WIDGET_REALIZED(decoration))
		gtk_widget_queue_resize(GTK_WIDGET(decoration));
}


static void
real_set_min_size(GwmDecoration * decoration, 
		  gint min_width, gint min_height)
{
}


static void
real_set_max_size(GwmDecoration * decoration, 
		  gint max_width, gint max_height)
{
}


static void
real_set_size_incriment(GwmDecoration * decoration,
				  gint width_inc, gint height_inc)
{
}


static void
real_set_aspect_ratio(GwmDecoration * decoration,
		      gfloat min_aspect, gfloat max_aspect)
{
}


static void
real_set_maximized_size(GwmDecoration * decoration,
			gint maximized_width, gint maximized_height)
{
}


static void
real_child_geometry(GwmDecoration * decoration,
		    GwmGeometry * geometry)
{
	gint width, height, resize_border_width, resize_border_width2, 
		titlebar_height;

	width = GTK_WIDGET(decoration)->allocation.width;
	height = GTK_WIDGET(decoration)->allocation.height;
	titlebar_height = decoration->titlebar_height;
	resize_border_width = decoration->resize_border_width;
	resize_border_width2 = resize_border_width * 2;
	
	if (!GWM_HINTS_HAS_BORDER(decoration->hints)) {
		geometry->x = 0;
		geometry->y = 0;
		geometry->width = width;
		geometry->height = height;

		return;
	}

	geometry->x = resize_border_width;
	geometry->y = resize_border_width + titlebar_height;
	geometry->width = width - resize_border_width2;
	geometry->height = height - resize_border_width2 - titlebar_height;
}


static void
real_title_geometry(GwmDecoration * decoration, 
		    GwmGeometry * geometry)
{
	gint width, height, side, resize_border_width, titlebar_height;

	width = GTK_WIDGET(decoration)->allocation.width;
	height = GTK_WIDGET(decoration)->allocation.height;
	resize_border_width = decoration->resize_border_width;
	titlebar_height = decoration->titlebar_height;

	/* this is the size of one of the buttons */
	side = titlebar_height - resize_border_width;

	geometry->x = (2 * resize_border_width) + side;
	geometry->y = (resize_border_width / 2) - 2;
	geometry->width = width - (2 * side) - (4 * resize_border_width);
	geometry->height = titlebar_height;
}


static void
real_close_button_geometry(GwmDecoration * decoration, 
			   GwmGeometry * geometry)
{
	gint button_size;

	button_size = decoration->titlebar_height - 8;

	geometry->x = decoration->resize_border_width + 4;
	geometry->y = decoration->resize_border_width + 4;
	geometry->width = button_size;
	geometry->height = button_size;
}


static void
real_iconify_button_geometry(GwmDecoration * decoration,
			     GwmGeometry * geometry)
{
	gint width, height;

	width = GTK_WIDGET(decoration)->allocation.width;
	height = GTK_WIDGET(decoration)->allocation.height;

	geometry->x = 0;
	geometry->y = 0;
	geometry->width = 1;
	geometry->height = 1;
}


static void
real_maximize_button_geometry(GwmDecoration * decoration, 
			      GwmGeometry * geometry)
{
	gint width, height;

	width = GTK_WIDGET(decoration)->allocation.width;
	height = GTK_WIDGET(decoration)->allocation.height;

	geometry->x = 0;
	geometry->y = 0;
	geometry->width = 1;
	geometry->height = 1;
}


static void
real_draw_background(GwmDecoration * decoration, 
		     GwmGeometry * geometry,
		     GdkRectangle * area)
{
	gtk_draw_shadow(GTK_WIDGET(decoration)->style, 
			GTK_WIDGET(decoration)->window,
			GTK_STATE_NORMAL, GTK_SHADOW_OUT,
			0, 0,
			GTK_WIDGET(decoration)->allocation.width,
			GTK_WIDGET(decoration)->allocation.height);

	gtk_draw_box(GTK_WIDGET(decoration)->style, 
		     GTK_WIDGET(decoration)->window,
		     GTK_STATE_SELECTED,
		     GTK_SHADOW_ETCHED_OUT,
		     decoration->resize_border_width,
		     decoration->resize_border_width,
		     GTK_WIDGET(decoration)->allocation.width - 
		     (2 * decoration->resize_border_width),
		     GTK_WIDGET(decoration)->allocation.height - 
		     (2 * decoration->resize_border_width));
}


static void
real_draw_title(GwmDecoration * decoration, 
		GwmGeometry * geometry,
		GdkRectangle * area)
{
	gtk_draw_string(GTK_WIDGET(decoration)->style,
			GTK_WIDGET(decoration)->window,
			GTK_STATE_SELECTED,
			geometry->x,
			geometry->y + geometry->height,
			decoration->hints->title);
}


static void
real_draw_close_button(GwmDecoration * decoration, 
		       GwmGeometry * geometry,
		       GdkRectangle * area)
{
	gtk_draw_box(GTK_WIDGET(decoration)->style,
		     GTK_WIDGET(decoration)->window,
		     GTK_STATE_NORMAL,
		     GTK_SHADOW_ETCHED_OUT,
		     geometry->x,
		     geometry->y,
		     geometry->width,
		     geometry->height);
}


static void
real_draw_iconify_button(GwmDecoration * decoration, 
			 GwmGeometry * geometry,
			 GdkRectangle * area)
{
}


static void
real_draw_maximize_button(GwmDecoration * decoration, 
			  GwmGeometry * geometry,
			  GdkRectangle * area)
{
}

/**** GwmDecoration window methods ****/

static void
resize_windows_geometry(GwmDecoration * decoration, GwmGeometry * geometry)
{
	/* XXXXX WARNING XXXXXX
	 * now, this is some wild code; you see, the array of geometry
	 * structures passed into this function needs to be filled in the
	 * order defined in the GwmDecorationWindows structure
	 *
	 * kill me now --JMP */

	gint width, height, resize_border_width, corner_resize_length, 
		tb_side_width, lr_side_height;

	width = GTK_WIDGET(decoration)->allocation.width;
	height = GTK_WIDGET(decoration)->allocation.height;

	resize_border_width = decoration->resize_border_width;
	corner_resize_length = decoration->corner_resize_length;
	tb_side_width = width - (2 * decoration->corner_resize_length);
	lr_side_height = height - (2 * decoration->corner_resize_length);

	/* tl1w */
	geometry->x = 0;
	geometry->y = 0;
	geometry->width = resize_border_width;
	geometry->height = corner_resize_length;

	geometry++;

	/* tl2w */
	geometry->x = 0;
	geometry->y = 0;
	geometry->width = corner_resize_length;
	geometry->height = resize_border_width;	

	geometry++;

	/* tw */
	geometry->x = corner_resize_length;
	geometry->y = 0;
	geometry->width = tb_side_width;
	geometry->height = resize_border_width;	
	
	geometry++;

	/* tr1w */
	geometry->x = tb_side_width + corner_resize_length;
	geometry->y = 0;
	geometry->width = corner_resize_length;
	geometry->height = resize_border_width;

	geometry++;

	/* tr2w */
	geometry->x = width - resize_border_width;
	geometry->y = 0;
	geometry->width = resize_border_width;
	geometry->height = corner_resize_length;

	geometry++;

	/* rw */
	geometry->x = width - resize_border_width;
	geometry->y = corner_resize_length;
	geometry->width = resize_border_width;
	geometry->height = lr_side_height;	

	geometry++;

	/* br1w */
	geometry->x = width - resize_border_width;
	geometry->y = height - corner_resize_length;
	geometry->width = resize_border_width;
	geometry->height = corner_resize_length;	
	
	geometry++;

	/* br2w */
	geometry->x = width - corner_resize_length;
	geometry->y = height - resize_border_width;
	geometry->width = corner_resize_length;
	geometry->height = resize_border_width;

	geometry++;

	/* bw */
	geometry->x = corner_resize_length;
	geometry->y = height - resize_border_width;
	geometry->width = tb_side_width;
	geometry->height = resize_border_width;

	geometry++;

	/* bl1w */
	geometry->x = 0;
	geometry->y = height - resize_border_width;
	geometry->width = corner_resize_length;
	geometry->height = resize_border_width;

	geometry++;

	/* bl2w */
	geometry->x = 0;
	geometry->y = height - corner_resize_length;
	geometry->width = resize_border_width;
	geometry->height = corner_resize_length;

	geometry++;

	/* lw */
	geometry->x = 0;
	geometry->y = corner_resize_length;
	geometry->width = resize_border_width;
	geometry->height = lr_side_height;
}


static void
resize_windows_position(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;
	GwmGeometry *geometry, geom_array[GWM_DECORATION_RESIZE_WINDOWS_COUNT];

	geometry = geom_array;
	ws = &decoration->windows;

	resize_windows_geometry(decoration, geometry);

	gdk_window_move_resize(
		ws->tl1w, 
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;
	
	gdk_window_move_resize(
		ws->tl2w,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->tw,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->tr1w,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->tr2w,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->rw,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->br1w,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->br2w,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->bw,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->bl1w,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->bl2w,
		geometry->x, geometry->y, geometry->width, geometry->height);
	geometry++;

	gdk_window_move_resize(
		ws->lw,
		geometry->x, geometry->y, geometry->width, geometry->height);
}


static void
resize_windows_create(GwmDecoration * decoration)
{
	GdkWindow *parent;
	gint attributes_mask;
	GdkWindowAttr attributes;
	GwmDecorationWindows *ws;
	GwmDecorationCursors *cs;
	GwmGeometry *geometry, geom_array[GWM_DECORATION_RESIZE_WINDOWS_COUNT];

	geometry = geom_array;
	parent = GTK_WIDGET(decoration)->window;
	ws = &decoration->windows;
	cs = &decoration->cursors;

	resize_windows_geometry(decoration, geometry);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.wclass = GDK_INPUT_ONLY;
	attributes.event_mask = GDK_BUTTON_PRESS_MASK|GDK_BUTTON_RELEASE_MASK;
	attributes_mask = GDK_WA_X|GDK_WA_Y|GDK_WA_CURSOR;


	/* cursors */
	cs->tlc = gdk_cursor_new(GDK_TOP_LEFT_CORNER);
	cs->tc = gdk_cursor_new(GDK_TOP_SIDE);
	cs->trc = gdk_cursor_new(GDK_TOP_RIGHT_CORNER);
	cs->rc = gdk_cursor_new(GDK_RIGHT_SIDE);
	cs->brc = gdk_cursor_new(GDK_BOTTOM_RIGHT_CORNER);
	cs->bc = gdk_cursor_new(GDK_BOTTOM_SIDE);
	cs->blc = gdk_cursor_new(GDK_BOTTOM_LEFT_CORNER);
	cs->lc = gdk_cursor_new(GDK_LEFT_SIDE);

	/* top left 1 corner */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->tlc;
	ws->tl1w = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->tl1w, decoration);

	/* top left corner 2 */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->tlc;
	ws->tl2w = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->tl2w, decoration);

	/* top */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->tc;
	ws->tw = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->tw, decoration);

	/* top right 1 corner */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->trc;
	ws->tr1w = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->tr1w, decoration);

	/* top right 2 corner */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->trc;
	ws->tr2w = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->tr2w, decoration);

	/* right */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->rc;
	ws->rw = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->rw, decoration);

	/* bottom right 1 */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->brc;
	ws->br1w = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->br1w, decoration);

	/* bottom right 2 */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->brc;
	ws->br2w = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->br2w, decoration);

	/* bottom */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->bc;
	ws->bw = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->bw, decoration);

	/* bottom left 1 */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->blc;
	ws->bl1w = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->bl1w, decoration);

	/* bottom left 2 */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->blc;
	ws->bl2w = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->bl2w, decoration);

	/* left */
	attributes.x = geometry->x;
	attributes.y = geometry->y;
	attributes.width = geometry->width;
	attributes.height = geometry->height;
	geometry++;

	attributes.cursor = cs->lc;
	ws->lw = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->lw, decoration);
}


static void
resize_windows_destroy(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;
	GwmDecorationCursors *cs;

	ws = &decoration->windows;
	cs = &decoration->cursors;

	/* destroy cursors */
	gdk_cursor_destroy(cs->tlc);
	gdk_cursor_destroy(cs->tc);
	gdk_cursor_destroy(cs->trc);
	gdk_cursor_destroy(cs->rc);
	gdk_cursor_destroy(cs->brc);
	gdk_cursor_destroy(cs->bc);
	gdk_cursor_destroy(cs->blc);
	gdk_cursor_destroy(cs->lc);
	cs->tlc = NULL;
	cs->tc = NULL;
	cs->trc = NULL;
	cs->rc = NULL;
	cs->brc = NULL;
	cs->bc = NULL;
	cs->blc = NULL;
	cs->lc = NULL;

	/* destroy windows */
	gdk_window_destroy(ws->tl1w);
	gdk_window_destroy(ws->tl2w);
	gdk_window_destroy(ws->tw);
	gdk_window_destroy(ws->tr1w);
	gdk_window_destroy(ws->tr2w);
	gdk_window_destroy(ws->rw);
	gdk_window_destroy(ws->br1w);
	gdk_window_destroy(ws->br2w);
	gdk_window_destroy(ws->bw);
	gdk_window_destroy(ws->bl1w);
	gdk_window_destroy(ws->bl2w);
	gdk_window_destroy(ws->lw);
	ws->tl1w = NULL;
	ws->tl2w = NULL;
	ws->tw = NULL;
	ws->tr1w = NULL;
	ws->tr2w = NULL;
	ws->rw = NULL;
	ws->br1w = NULL;
	ws->br2w = NULL;
	ws->bw = NULL;
	ws->bl1w = NULL;
	ws->bl2w = NULL;
	ws->lw = NULL;
}


static void
resize_windows_map(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;

	gdk_window_show(ws->tl1w);
	gdk_window_show(ws->tl2w);
	gdk_window_show(ws->tw);
	gdk_window_show(ws->tr1w);
	gdk_window_show(ws->tr2w);
	gdk_window_show(ws->rw);
	gdk_window_show(ws->br1w);
	gdk_window_show(ws->br2w);
	gdk_window_show(ws->bw);
	gdk_window_show(ws->bl1w);
	gdk_window_show(ws->bl2w);
	gdk_window_show(ws->lw);
}


static void
resize_windows_unmap(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;

	gdk_window_hide(ws->tl1w);
	gdk_window_hide(ws->tl2w);
	gdk_window_hide(ws->tw);
	gdk_window_hide(ws->tr1w);
	gdk_window_hide(ws->tr2w);
	gdk_window_hide(ws->rw);
	gdk_window_hide(ws->br1w);
	gdk_window_hide(ws->br2w);
	gdk_window_hide(ws->bw);
	gdk_window_hide(ws->bl1w);
	gdk_window_hide(ws->bl2w);
	gdk_window_hide(ws->lw);
}


static void
close_window_position(GwmDecoration * decoration)
{
	GwmGeometry geometry;
	GwmDecorationWindows *ws;
	GwmDecorationCursors *cs;

	ws = &decoration->windows;
	cs = &decoration->cursors;
	
	gwm_decoration_close_button_geometry(decoration, &geometry);

	gdk_window_move_resize(
		ws->cw, 
		geometry.x, geometry.y, geometry.width, geometry.height);
}


static void
close_window_create(GwmDecoration * decoration)
{
	GdkWindow *parent;
	gint attributes_mask;
	GdkWindowAttr attributes;
	GwmGeometry geometry;
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	parent = GTK_WIDGET(decoration)->window;

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.wclass = GDK_INPUT_ONLY;
	attributes.event_mask = GDK_BUTTON_PRESS_MASK|GDK_BUTTON_RELEASE_MASK;
	attributes_mask = GDK_WA_X | GDK_WA_Y;

	gwm_decoration_close_button_geometry(decoration, &geometry);

	attributes.x = geometry.x;
	attributes.y = geometry.y;
	attributes.width = geometry.width;
	attributes.height = geometry.height;

	ws->cw = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->cw, decoration);
}


static void
close_window_destroy(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_destroy(ws->cw);
	ws->cw = NULL;
}


static void
close_window_map(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_show(ws->cw);
}


static void
close_window_unmap(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_hide(ws->cw);
}


static void
iconify_window_position(GwmDecoration * decoration)
{
	GwmGeometry geometry;
	GwmDecorationWindows *ws;
	GwmDecorationCursors *cs;

	ws = &decoration->windows;
	cs = &decoration->cursors;
	
	gwm_decoration_iconify_button_geometry(decoration, &geometry);

	gdk_window_move_resize(
		ws->iw,
		geometry.x, geometry.y, geometry.width, geometry.height);
}


static void
iconify_window_create(GwmDecoration * decoration)
{
	GdkWindow *parent;
	gint attributes_mask;
	GdkWindowAttr attributes;
	GwmGeometry geometry;
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	parent = GTK_WIDGET(decoration)->window;

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.wclass = GDK_INPUT_ONLY;
	attributes.event_mask = GDK_BUTTON_PRESS_MASK|GDK_BUTTON_RELEASE_MASK;
	attributes_mask = GDK_WA_X | GDK_WA_Y;

	gwm_decoration_iconify_button_geometry(decoration, &geometry);

	attributes.x = geometry.x;
	attributes.y = geometry.y;
	attributes.width = geometry.width;
	attributes.height = geometry.height;

	ws->iw = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->iw, decoration);
}


static void
iconify_window_destroy(GwmDecoration * decoration)
{	
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_destroy(ws->iw);
	ws->iw = NULL;
}


static void
iconify_window_map(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_show(ws->iw);
}


static void
iconify_window_unmap(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_hide(ws->iw);
}


static void
maximize_window_position(GwmDecoration * decoration)
{
	GwmGeometry geometry;
	GwmDecorationWindows *ws;
	GwmDecorationCursors *cs;

	ws = &decoration->windows;
	cs = &decoration->cursors;
	
	gwm_decoration_maximize_button_geometry(decoration, &geometry);

	gdk_window_move_resize(
		ws->mw,
		geometry.x, geometry.y, geometry.width, geometry.height);
}


static void
maximize_window_create(GwmDecoration * decoration)
{
	GdkWindow *parent;
	gint attributes_mask;
	GdkWindowAttr attributes;
	GwmGeometry geometry;
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	parent = GTK_WIDGET(decoration)->window;

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.wclass = GDK_INPUT_ONLY;
	attributes.event_mask = GDK_BUTTON_PRESS_MASK|GDK_BUTTON_RELEASE_MASK;
	attributes_mask = GDK_WA_X | GDK_WA_Y;

	gwm_decoration_maximize_button_geometry(decoration, &geometry);

	attributes.x = geometry.x;
	attributes.y = geometry.y;
	attributes.width = geometry.width;
	attributes.height = geometry.height;

	ws->mw = gdk_window_new(parent, &attributes, attributes_mask);
	gdk_window_set_user_data(ws->mw, decoration);
}


static void
maximize_window_destroy(GwmDecoration * decoration)
{	
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_destroy(ws->mw);
	ws->mw = NULL;
}


static void
maximize_window_map(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_show(ws->mw);
}


static void
maximize_window_unmap(GwmDecoration * decoration)
{
	GwmDecorationWindows *ws;

	ws = &decoration->windows;
	gdk_window_hide(ws->mw);
}

/**** GwmHints Callbacks ****/

static void
hints_title_changed_cb(GwmHints * hints, gchar * title, 
		       GwmDecoration * decoration)
{
	gwm_decoration_set_title(decoration, title);
}


static void
hints_icon_title_changed_cb(GwmHints * hints, gchar * title, 
			    GwmDecoration * decoration)
{
	gwm_decoration_set_icon_title(decoration, title);
}


static void 
hints_position_changed_cb(GwmHints * hints, 
			  gint x, gint y, GwmDecoration * decoration)
{
	gwm_decoration_set_position(decoration, x, y);
}


static void
hints_min_size_changed_cb(GwmHints * hints, gint min_width, 
			  gint min_height, GwmDecoration * decoration)
{
	gwm_decoration_set_min_size(decoration, min_width, min_height);
}


static void
hints_max_size_changed_cb(GwmHints * hints, gint max_width, 
			  gint max_height, GwmDecoration * decoration)
{
	gwm_decoration_set_max_size(decoration, max_width, max_height);
}


static void
hints_size_incriment_changed_cb(GwmHints * hints, gint width_inc, 
				gint height_inc, GwmDecoration * decoration)
{
	gwm_decoration_set_size_incriment(decoration, width_inc, height_inc);
}


static void
hints_aspect_ratio_changed_cb(GwmHints * hints, gfloat min_aspect, 
			      gfloat max_aspect, GwmDecoration * decoration)
{
	gwm_decoration_set_aspect_ratio(decoration, min_aspect, max_aspect);
}

