/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* CONVENTION NOTE: except for error trap functions, try to do everything
 *                  in here with Xlib calls and not gdk calls! --JMP 
 */

#include "gnome-hints.h"


/* states set by the window manager */

#define WIN_STATE_STICKY          (1<<0)    /* everyone knows sticky */
#define WIN_STATE_RESERVED_BIT1   (1<<1)    /* removed minimize here */
#define WIN_STATE_MAXIMIZED_VERT  (1<<2)    /* window in maximized V state */
#define WIN_STATE_MAXIMIZED_HORIZ (1<<3)    /* window in maximized H state */
#define WIN_STATE_HIDDEN          (1<<4)    /* not on taskbar but window 
					     * visible */
#define WIN_STATE_SHADED          (1<<5)    /* shaded (NeXT style) */
#define WIN_STATE_HID_WORKSPACE   (1<<6)    /* not on current desktop */
#define WIN_STATE_HID_TRANSIENT   (1<<7)    /* owner of transient is hidden */
#define WIN_STATE_FIXED_POSITION  (1<<8)    /* window is fixed in position 
					     * even */
#define WIN_STATE_ARRANGE_IGNORE  (1<<9)    /* ignore for auto arranging
					     * when scrolling about large
					     * virtual desktops ala fvwm */

/* hints set by the client */

#define WIN_HINTS_SKIP_FOCUS      (1<<0)    /* "alt-tab" skips this win */
#define WIN_HINTS_SKIP_WINLIST    (1<<1)    /* not in win list */
#define WIN_HINTS_SKIP_TASKBAR    (1<<2)    /* not on taskbar */
#define WIN_HINTS_GROUP_TRANSIENT (1<<3)    /* ??????? */
#define WIN_HINTS_FOCUS_ON_CLICK  (1<<4)    /* app only accepts focus when 
					     * clicked */
#define WIN_HINTS_DO_NOT_COVER    (1<<5)    /* attempt to not cover this 
					     * window */


void
gnome_hints_load(GwmHints * hints, Window xwindow)
{
}


gboolean
gnome_hints_process_client_property(GwmHints * hints, XEvent * xevent)
{
	return FALSE;
}
