/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gtk/gtkprivate.h>
#include <gdk/gdkx.h>
#include <X11/Xatom.h>

#include "debug.h"
#include "xatoms.h"
#include "gwmcontext.h"

static void gwm_context_class_init              (GwmContextClass * class);
static void gwm_context_init                    (GwmContext * context);
static void map_request_cb                      (GwmRoot * root,
						 guint32 xid, 
						 GwmContext * context);
static void client_destroy_cb                   (GwmClient * client,
						 GwmContext * context);
static void client_show_cb                      (GwmClient * client,
						 GwmContext * context);
static void client_hide_cb                      (GwmClient * client,
						 GwmContext * context);

static void decoration_close_cb                 (GwmDecoration * decoration,
						 GwmContext * context);
static void decoration_enter_notify_cb          (GwmDecoration * decoration,
						 GdkEventCrossing * event,
						 GwmContext * context);
static void decoration_leave_notify_cb          (GwmDecoration * decoration,
						 GdkEventCrossing * event,
						 GwmContext * context);


static GtkObjectClass *parent_class = NULL;


GtkType
gwm_context_get_type(void)
{
	static GtkType context_type = 0;

	if (!context_type) {
		static const GtkTypeInfo context_info = {
			"GwmContext",
			sizeof(GwmContext),
			sizeof(GwmContextClass),
			(GtkClassInitFunc) gwm_context_class_init,
			(GtkObjectInitFunc) gwm_context_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
		
		context_type = gtk_type_unique(
			gtk_object_get_type(), &context_info);
	}

	return context_type;
}


static void
gwm_context_class_init(GwmContextClass * class)
{
	parent_class = gtk_type_class(gtk_object_get_type());
}


static void
gwm_context_init(GwmContext * context)
{
	context->xid = 0;
	context->root = NULL;
	context->client_list = NULL;
	context->focus_client = NULL;
}


GwmContext *
gwm_context_new()
{
	GwmContext *context;
	context = gtk_type_new(gwm_context_get_type());
	return context;
}


gboolean
gwm_context_manage_xid(GwmContext * context, guint32 xid)
{
	GdkWindow *window;
	XWindowAttributes xattrs;

	DEBUG_MESSAGE("gwm_context_manage_xid");

	g_return_val_if_fail(context != NULL, FALSE);

	context->xid = xid;

	/* wrap the XID in a GdkWindow */
	window = gdk_window_lookup(xid);
	if (!window)
		window = gdk_window_foreign_new(xid);

	/* select for redirection events */
	gdk_error_trap_push();

	XGetWindowAttributes(GDK_DISPLAY(), xid, &xattrs);
	xattrs.your_event_mask |= SubstructureRedirectMask;
	XSelectInput(GDK_DISPLAY(), xid, xattrs.your_event_mask);
	gdk_flush();

	if (gdk_error_trap_pop())
		return FALSE;

	/* create the root container for the context */
	context->root = (GwmRoot *) gwm_root_new(window);

	gtk_signal_connect(GTK_OBJECT(context->root),
			   "map_request",
			   map_request_cb,
			   context);

	gtk_widget_show(GTK_WIDGET(context->root));

	return TRUE;
}


void
gwm_context_capture_existing_windows(GwmContext * context)
{
	unsigned int i, n;
	Window xid_root, xid_parent, *xid_wlist;

	if (!XQueryTree(GDK_DISPLAY(), context->xid, &xid_root, 
			&xid_parent, &xid_wlist, &n))
		return;

	/* iterate over the windows, caputre any windows not on the GDK 
	 * lookup table */
	for (i = 0; i < n; i++) {
		XWindowAttributes xattr;

		if (gdk_window_lookup(xid_wlist[i]))
			continue;

		if (!XGetWindowAttributes(GDK_DISPLAY(), xid_wlist[i], &xattr))
			continue;

		if (xattr.map_state == IsUnmapped)
			continue;

		if (xattr.override_redirect)
			continue;
		
		gwm_context_capture_xid(context, xid_wlist[i]);
	}

	/* free the memory alloated by XQueryTree */
	if (n > 0)
		XFree((char *) xid_wlist);
}


void
gwm_context_capture_xid(GwmContext * context, guint32 xid)
{
	GList *list;
	GwmClient *client;
	GwmDecoration *decoration;

	DEBUG_MESSAGE("capture_xid");

	/* make sure not to capture a window we've already captured */
	list = context->client_list;
	while (list) {
		guint32 client_xid;

		client = list->data;
		list = list->next;

		client_xid = GDK_WINDOW_XWINDOW(client->client_window);

		if (client_xid == xid) {
			DEBUG_MESSAGE("already captured");
			return;
		}
	}

	/* allocate a client widget and have it capture the window */
	client = gwm_client_new(NULL);
	context->client_list = g_list_append(context->client_list, client);

	gtk_signal_connect(GTK_OBJECT(client),
			   "destroy",
			   client_destroy_cb,
			   context);

	gtk_signal_connect(GTK_OBJECT(client),
			   "show",
			   client_show_cb,
			   context);

	gtk_signal_connect(GTK_OBJECT(client),
			   "hide",
			   client_hide_cb,
			   context);

	/* now attempt to capture the window */
	if (!gwm_client_capture(client, xid)) {
		DEBUG_MESSAGE("failed to capture");
		gtk_widget_destroy(GTK_WIDGET(client));
		return;
	}

	decoration = gwm_decoration_new(client->hints);

	gtk_container_add(GTK_CONTAINER(decoration),
			  GTK_WIDGET(client));
	gtk_container_add(GTK_CONTAINER(context->root),
			  GTK_WIDGET(decoration));
	       
	gtk_signal_connect(GTK_OBJECT(decoration),
			   "close",
			   decoration_close_cb,
			   context);

	gtk_signal_connect(GTK_OBJECT(decoration),
			   "enter_notify_event",
			   decoration_enter_notify_cb,
			   context);
	
	gtk_signal_connect(GTK_OBJECT(decoration),
			   "leave_notify_event",
			   decoration_leave_notify_cb,
			   context);
	
	gtk_widget_show(GTK_WIDGET(client));
	gtk_widget_show(GTK_WIDGET(decoration));
}


/* GwmRoot callbacks */
static void
map_request_cb(GwmRoot * root, guint32 xid, GwmContext * context)
{
	DEBUG_MESSAGE("map_request_cb");
	gwm_context_capture_xid(context, xid);
}



/* GwmClient callbacks */
static void
client_destroy_cb(GwmClient * client, GwmContext * context)
{
	GtkWidget *parent;

	DEBUG_MESSAGE("client_destroy_cb");

	g_return_if_fail(client != NULL);
	g_return_if_fail(GWM_IS_CLIENT(client));
	g_return_if_fail(context != NULL);

	/* if the client's parent is a GwmDecoration, then destroy it */
	parent = GTK_WIDGET(client)->parent;
	if (GWM_IS_DECORATION(parent))
		gtk_widget_destroy(parent);

	context->client_list = g_list_remove(context->client_list, client);
}


static void
client_show_cb(GwmClient * client, GwmContext * context)
{
	GtkWidget *parent;

	DEBUG_MESSAGE("client_show_cb");

	g_return_if_fail(client != NULL);
	g_return_if_fail(GWM_IS_CLIENT(client));
	g_return_if_fail(context != NULL);

	parent = GTK_WIDGET(client)->parent;
	if (GWM_IS_DECORATION(parent))
		gtk_widget_show(parent);
}


static void
client_hide_cb(GwmClient * client, GwmContext * context)
{
	GtkWidget *parent;

	DEBUG_MESSAGE("client_hide_cb");
	
	g_return_if_fail(client != NULL);
	g_return_if_fail(GWM_IS_CLIENT(client));
	g_return_if_fail(context != NULL);

	parent = GTK_WIDGET(client)->parent;
	if (GWM_IS_DECORATION(parent))
		gtk_widget_hide(parent);
}


/* GwmDecoration callbacks */
static void
decoration_close_cb(GwmDecoration * decoration, GwmContext * context)
{
	GtkWidget *child;

	child = GTK_BIN(decoration)->child;
	if (GWM_IS_CLIENT(child))
		gwm_client_close(GWM_CLIENT(child));
}


static void
decoration_enter_notify_cb(GwmDecoration * decoration, 
			   GdkEventCrossing * event,
			   GwmContext * context)
{
	GtkWidget *child;

	/* XXX: add features click-to-focus and focus-follows-mouse
	 * 
	 * maybe the best thing to do is to disconnect this signal
	 * when in click-to-focus mode so we have less overhead -JMP */

	child = GTK_BIN(decoration)->child;
	if (GWM_IS_CLIENT(child))
		gwm_client_take_focus(GWM_CLIENT(child));
}


static void
decoration_leave_notify_cb(GwmDecoration * decoration, 
			   GdkEventCrossing * event,
			   GwmContext * context)
{
	/* XXX: need to release focus -JMP */
}

