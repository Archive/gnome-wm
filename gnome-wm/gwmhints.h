/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __gwm_hints_h__
#define __gwm_hints_h__

#include <gdk/gdkx.h>
#include <gtk/gtk.h>

/* decoration flags */
enum {
	GWM_HINTS_DECOR_BORDER              = 1 <<  0,
	GWM_HINTS_DECOR_RESIZEH             = 1 <<  1,
	GWM_HINTS_DECOR_TITLE               = 1 <<  2,
	GWM_HINTS_DECOR_MENU                = 1 <<  3,
	GWM_HINTS_DECOR_MINIMIZE            = 1 <<  4,
	GWM_HINTS_DECOR_MAXIMIZE            = 1 <<  5,
}; 

#define GWM_TYPE_HINTS\
        (gwm_hints_get_type())

#define GWM_HINTS(obj)\
        (GTK_CHECK_CAST((obj), GWM_TYPE_HINTS, GwmHints))

#define GWM_HINTS_CLASS(klass)\
        (GTK_CHECK_CLASS_CAST((klass), GWM_TYPE_HINTS, GwmHintsClass))

#define GWM_IS_HINTS(obj)\
        (GTK_CHECK_TYPE((obj), GWM_TYPE_HINTS))

#define GWM_IS_HINTS_CLASS(klass)\
        (GTK_CHECK_CLASS_TYPE((klass), GWM_TYPE_HINTS))

/* macros to aid decoration flag comparisons */
#define GWM_HINTS_HAS_BORDER(hints)\
        ((hints)->decoration_flags & GWM_HINTS_DECOR_BORDER)

#define GWM_HINTS_HAS_RESIZEH(hints)\
        ((hints)->decoration_flags & GWM_HINTS_DECOR_RESIZEH)

#define GWM_HINTS_HAS_TITLE(hints)\
        ((hints)->decoration_flags & GWM_HINTS_DECOR_TITLE)

#define GWM_HINTS_HAS_MENU(hints)\
        ((hints)->decoration_flags & GWM_HINTS_DECOR_MENU)

#define GWM_HINTS_HAS_MINIMIZE(hints)\
        ((hints)->decoration_flags & GWM_HINTS_DECOR_MINIMIZE)

#define GWM_HINTS_HAS_MAXIMIZE(hints)\
        ((hints)->decoration_flags & GWM_HINTS_DECOR_MAXIMIZE)

typedef struct _GwmHints GwmHints;
typedef struct _GwmHintsClass GwmHintsClass;

struct _GwmHints {
	GtkObject object;

	gchar *title;
	gchar *icon_title;

	gboolean take_focus, save_yourself, delete_window;

	gint x, y, width, height;
	gint border_width;

	gint min_width, min_height;
	gint max_width, max_height;
	gint width_inc, height_inc;
	gfloat min_aspect, max_aspect;
	gint maximized_width, maximized_height;

	guint decoration_flags;
};

struct _GwmHintsClass {
	GtkObjectClass parent_class;

	void (*take_focus_changed)             (GwmHints * hints,
						gboolean take_focus);
	void (*save_yourself_changed)          (GwmHints * hints,
						gboolean save_yourself);
	void (*delete_window_changed)          (GwmHints * hints,
						gboolean delete_window);
	void (*title_changed)                  (GwmHints * hints,
						gchar * title);
	void (*icon_title_changed)             (GwmHints * hints,
						gchar * title);
	void (*size_changed)                   (GwmHints * hints,
						gint width,
						gint height);
	void (*position_changed)               (GwmHints * hints,
						gint x,
						gint y);
	void (*border_width_changed)           (GwmHints * hints,
						gint border_width);
	void (*min_size_changed)               (GwmHints * hints,
						gint min_width, 
						gint min_height);
	void (*max_size_changed)               (GwmHints * hints,
						gint max_width,
						gint max_height);
	void (*size_incriment_changed)         (GwmHints * hints,
						gint width_inc,
						gint height_inc);
	void (*aspect_ratio_changed)           (GwmHints * hints,
						gfloat min_aspect,
						gfloat max_aspect);
	void (*maximized_size_changed)         (GwmHints * hints,
						gint maximized_width,
						gint maximized_height);
	void (*decoration_changed)             (GwmHints * hints,
						guint decoration_flags);

};


GtkType      gwm_hints_get_type                (void);
GwmHints *   gwm_hints_new                     ();
void         gwm_hints_load                    (GwmHints * hints, 
						Window xwindow);
gboolean     gwm_hints_process_property_notify (GwmHints * hints,
						XEvent * event);
void         gwm_hints_set_take_focus          (GwmHints * hints,
						gboolean take_focus);
void         gwm_hints_set_save_yourself       (GwmHints * hints,
						gboolean save_yourself);
void         gwm_hints_set_delete_window       (GwmHints * hints,
						gboolean delete_window);
void         gwm_hints_set_title               (GwmHints * hints, 
						gchar * title);
void         gwm_hints_set_icon_title          (GwmHints * hints, 
						gchar * title);
void         gwm_hints_set_size                (GwmHints * hints,
						gint width,
						gint height);
void         gwm_hints_set_position            (GwmHints * hints, 
						gint x, 
						gint y);
void         gwm_hints_set_border_width        (GwmHints * hints,
						gint border_width);
void         gwm_hints_set_min_size            (GwmHints * hints, 
						gint min_width,
						gint min_height);
void         gwm_hints_set_max_size            (GwmHints * hints, 
						gint max_width, 
						gint max_height);
void         gwm_hints_set_size_incriment      (GwmHints * hints, 
						gint width_inc, 
						gint height_inc);
void         gwm_hints_set_aspect_ratio        (GwmHints * hints, 
						gfloat min_aspect, 
						gfloat max_aspect);
void         gwm_hints_set_maximized_size      (GwmHints * hints,
						gint maximized_width,
						gint maximized_height);
void         gwm_hints_set_decoration_flags    (GwmHints * hints, 
						guint decoration_flags);

#endif /* __gwm_hints_h__ */
