/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gtk/gtkprivate.h>
#include <gdk/gdkx.h>
#include <X11/Xatom.h>
#include <gtk/gtksignal.h>

#include "debug.h"
#include "xatoms.h"
#include "gwmroot.h"


#define DEFAULT_LAYER 4


static void gwm_root_class_init                (GwmRootClass * class);
static void gwm_root_init                      (GwmRoot * mwindow);
static void gwm_root_realize                   (GtkWidget * widget);
static void gwm_root_unrealize                 (GtkWidget * widget);
static void gwm_root_map                       (GtkWidget * widget);
static void gwm_root_unmap                     (GtkWidget * widget);
static void gwm_root_show                      (GtkWidget * widget);
static void gwm_root_size_request              (GtkWidget * widget,
						GtkRequisition * requisition);
static void gwm_root_size_allocate             (GtkWidget * widget, 
						GtkAllocation * allocation);
static void gwm_root_draw                      (GtkWidget * widget,
						GdkRectangle * area);
static void gwm_root_add                       (GtkContainer * container, 
						GtkWidget *widget);
static void gwm_root_remove                    (GtkContainer * container,
						GtkWidget *widget);
static void gwm_root_forall                    (GtkContainer * container,
						gboolean include_internals,
						GtkCallback callback,
						gpointer callback_data);
GdkFilterReturn root_filter_func               (GdkXEvent * gdk_xevent,
						GdkEvent * event,
						gpointer data);



enum {
	MAP_REQUEST,
	LAST_SIGNAL
};

static guint root_signals[LAST_SIGNAL] = { 0 };
static GtkContainerClass *parent_class = NULL;

GtkType
gwm_root_get_type(void)
{
	static GtkType root_type = 0;

	if (!root_type) {
		static const GtkTypeInfo root_info = {
			"GwmRoot",
			sizeof(GwmRoot),
			sizeof(GwmRootClass),
			(GtkClassInitFunc) gwm_root_class_init,
			(GtkObjectInitFunc) gwm_root_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		root_type = gtk_type_unique(gtk_container_get_type(),
					    &root_info);
	}

	return root_type;
}


static void
gwm_root_class_init(GwmRootClass * class)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;
	GtkContainerClass *container_class;

	object_class = (GtkObjectClass *) class;
	widget_class = (GtkWidgetClass *) class;
	container_class = (GtkContainerClass *) class;
	parent_class = gtk_type_class(gtk_container_get_type());

	root_signals[MAP_REQUEST] = gtk_signal_new(
		"map_request",
		GTK_RUN_FIRST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmRootClass, map_request),
		gtk_marshal_NONE__UINT,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_UINT);
	
	gtk_object_class_add_signals(object_class, root_signals, LAST_SIGNAL);

	widget_class->realize = gwm_root_realize;
	widget_class->unrealize = gwm_root_unrealize;
	widget_class->map = gwm_root_map;
	widget_class->unmap = gwm_root_unmap;
	widget_class->show = gwm_root_show;
	widget_class->size_request = gwm_root_size_request;
	widget_class->size_allocate = gwm_root_size_allocate;
	widget_class->draw = gwm_root_draw;

	container_class->add = gwm_root_add;
	container_class->remove = gwm_root_remove;
	container_class->forall = gwm_root_forall;
}


static void
gwm_root_init(GwmRoot * root)
{
	GTK_WIDGET_UNSET_FLAGS(root, GTK_NO_WINDOW);
	GTK_WIDGET_SET_FLAGS(root, GTK_TOPLEVEL);

	gtk_container_set_resize_mode(GTK_CONTAINER(root),
				      GTK_RESIZE_QUEUE);

	gtk_container_register_toplevel(GTK_CONTAINER(root));

	root->children = NULL;

	root->viewport_size_x = 1;
	root->viewport_size_y = 1;
	root->viewport_offset_x = 0;
	root->viewport_offset_y = 0;
}


GtkWidget *
gwm_root_new(GdkWindow * window)
{
	GwmRoot *root;

	root = gtk_type_new(gwm_root_get_type());
	GTK_WIDGET(root)->window = window;
	gdk_window_add_filter(window, root_filter_func, root);

	return GTK_WIDGET(root);
}


void
gwm_root_add_layer(GwmRoot * root, GtkWidget * widget, gint layer)
{
	GwmRootChild *child_info;
	
	DEBUG_MESSAGE("gwm_root_add_layer");

	g_return_if_fail(root != NULL);
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(root));

	gtk_widget_set_parent(widget, GTK_WIDGET(root));

	child_info = g_new(GwmRootChild, 1);
	child_info->widget = widget;
	child_info->layer = layer;
	root->children = g_list_append(root->children, child_info);

	if (GTK_WIDGET_REALIZED(widget->parent))
		gtk_widget_realize(widget);

	if (GTK_WIDGET_VISIBLE(widget->parent) && GTK_WIDGET_VISIBLE(widget)) {
		if (GTK_WIDGET_MAPPED(widget->parent))
			gtk_widget_map(widget);

		gtk_widget_queue_resize(widget);
	}
}


void
gwm_root_set_viewport_size(GwmRoot * root, gint x, gint y)
{
	g_return_if_fail(root != NULL);
	g_return_if_fail(GWM_IS_ROOT(root));
	g_return_if_fail(x > 0);
	g_return_if_fail(y > 0);

	/* lots more stuff to do here; must set hints, and re-place
	 * windows which are off-screen if the viewport is shrunk -JMP */

	root->viewport_size_x = x;
	root->viewport_size_y = y;
}


void
gwm_root_set_viewport_offset(GwmRoot * root, gint x, gint y)
{
	g_return_if_fail(root != NULL);
	g_return_if_fail(GWM_IS_ROOT(root));
	g_return_if_fail(x > 0 && x <= root->viewport_size_x);
	g_return_if_fail(y > 0 && y <= root->viewport_size_y);

	/* must update window hints for pager here */

	root->viewport_offset_x = x;
	root->viewport_offset_y = y;
	gtk_widget_queue_resize(GTK_WIDGET(root));
}


static void
gwm_root_realize(GtkWidget * widget)
{
	GwmRoot *root;

	DEBUG_MESSAGE("gwm_root_realize");

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(widget));

	GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);
	root = GWM_ROOT(widget);

	gdk_window_set_user_data(widget->window, widget);
	widget->style = gtk_style_attach(widget->style, widget->window);
}


static void
gwm_root_unrealize(GtkWidget * widget)
{
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(widget));

	GTK_WIDGET_UNSET_FLAGS(widget, GTK_REALIZED);
}


static void
gwm_root_map(GtkWidget * widget)
{
	GwmRoot *root;
	GList *children;

	DEBUG_MESSAGE("gwm_root_map");

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(widget));

	root = GWM_ROOT(widget);
	GTK_WIDGET_SET_FLAGS(widget, GTK_MAPPED);
	
	children = root->children;
	while (children) {
		GwmRootChild *child_info;

		child_info = children->data;
		children = children->next;
		
		if (GTK_WIDGET_VISIBLE(child_info->widget) &&
		    !GTK_WIDGET_MAPPED(child_info->widget))
			gtk_widget_map(child_info->widget);
	}

}


static void
gwm_root_unmap(GtkWidget * widget)
{
	GwmRoot *root;
	GList *children;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(widget));

	root = GWM_ROOT(widget);
	GTK_WIDGET_UNSET_FLAGS(widget, GTK_MAPPED);

	children = root->children;
	while (children) {
		GwmRootChild *child_info;

		child_info = children->data;
		children = children->next;
		
		if (GTK_WIDGET_MAPPED(child_info->widget))
			gtk_widget_unmap(child_info->widget);
	}

}


static void
gwm_root_show(GtkWidget * widget)
{
	GwmRoot *root;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(widget));

	root = GWM_ROOT(widget);

	if (!GTK_WIDGET_VISIBLE(widget)) {
		GTK_WIDGET_SET_FLAGS(widget, GTK_VISIBLE);

		if (!GTK_WIDGET_MAPPED(widget))
			gtk_widget_map(widget);
	}
}


static void
gwm_root_size_request(GtkWidget * widget, GtkRequisition *requisition)
{
	GwmRoot *root;
	GList *children;
	
	DEBUG_MESSAGE("gwm_root_size_request");

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(widget));
	g_return_if_fail(requisition != NULL);

	root = GWM_ROOT(widget);

	requisition->width = 1;
	requisition->height = 1;

	if (GTK_WIDGET_REALIZED(widget)) {
		gint width, height;

		gdk_window_get_size(widget->window, &width, &height);
		requisition->width = width;
		requisition->height = height;

		DEBUG_MESSAGE("gwm_root_size_request: width=%d height=%d",
			  requisition->width, requisition->height);

	} else {
		GTK_CONTAINER(root)->need_resize = TRUE;
	}

	children = root->children;
	while (children) {
		GwmRootChild *child_info;
		GtkRequisition child_requisition;

		child_info = children->data;
		children = children->next;

		gtk_widget_size_request(child_info->widget, &child_requisition);
	}
}


static void
gwm_root_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
	GwmRoot *root;
	GList *children;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(widget));
	g_return_if_fail(allocation != NULL);

	DEBUG_MESSAGE("gwm_root_size_allocate: x=%d y=%d w=%d h=%d",
		  allocation->x,
		  allocation->y,
		  allocation->width,
		  allocation->height);

	root = GWM_ROOT(widget);
	widget->allocation = *allocation;

	children = root->children;
	while (children) {
		GwmRootChild *child_info;
		GtkAllocation child_allocation;
		GtkRequisition child_requisition;

		child_info = children->data;
		children = children->next;

		gtk_widget_get_child_requisition(child_info->widget, 
						 &child_requisition);

		child_allocation.x = 
			child_info->widget->allocation.x - 
			root->viewport_offset_x;

		child_allocation.y = 
			child_info->widget->allocation.y -
			root->viewport_offset_y;

		child_allocation.width = child_requisition.width;
		child_allocation.height = child_requisition.height;

		gtk_widget_size_allocate(child_info->widget, 
					 &child_allocation);
	}
}


static void
gwm_root_draw(GtkWidget *widget, GdkRectangle *area)
{
	GwmRoot *root;
	GList *children;

	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(widget));

	root = GWM_ROOT(widget);

	if (!GTK_WIDGET_DRAWABLE(widget))
		return;

	gdk_window_clear_area(
		widget->window, area->x, area->y, area->width, area->height);

	children = root->children;
	while (children) {
		GwmRootChild *child_info;
		GdkRectangle child_area;

		child_info = children->data;
		children = children->next;
		
		if (gtk_widget_intersect(child_info->widget, area, &child_area))
			gtk_widget_draw(child_info->widget, &child_area);
	}
}


static void
gwm_root_add(GtkContainer * container, GtkWidget * widget)
{
	GwmRoot *root;

	DEBUG_MESSAGE("gwm_root_add");

	g_return_if_fail(container != NULL);
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(container));

	root = GWM_ROOT(container);
	gwm_root_add_layer(root, widget, DEFAULT_LAYER);
}


static void
gwm_root_remove(GtkContainer * container, GtkWidget *widget)
{
	GwmRoot *root;
	GList *children;

	DEBUG_MESSAGE("gwm_root_remove");

	g_return_if_fail(container != NULL);
	g_return_if_fail(widget != NULL);
	g_return_if_fail(GWM_IS_ROOT(container));

	root = GWM_ROOT(container);

	children = root->children;
	while (children) {
		GwmRootChild *child_info;

		child_info = children->data;
		children = children->next;

		if (child_info->widget != widget)
			continue;

		root->children = g_list_remove(root->children, child_info);
		gtk_widget_unparent(child_info->widget);
		g_free(child_info);
		break;
	}
}


static void
gwm_root_forall(GtkContainer  * container,
		gboolean        include_internals,
		GtkCallback	 callback,
		gpointer	 callback_data)
{
	GwmRoot *root;
	GList *children;
	
	g_return_if_fail(container != NULL);
	g_return_if_fail(GWM_IS_ROOT(container));
	g_return_if_fail(callback != NULL);
	
	root = GWM_ROOT(container);
	children = root->children;
	
	while (children) {
		GwmRootChild *child_info;

		child_info = children->data;
		children = children->next;
		
		(* callback)(child_info->widget, callback_data);
	}
}


/* root window event handlers */
static GdkFilterReturn
root_configure_request(GwmRoot * root, GdkEvent * event, XEvent * xevent)
{
	unsigned int value_mask;
	XWindowChanges values;
	XConfigureRequestEvent *xconfigurerequest;
	Window xid;

	xconfigurerequest = &(xevent->xconfigurerequest);
	xid = xconfigurerequest->window;

	/* GDK will take care of windows it already knows about */
	if (gdk_window_lookup(xid)) {
		DEBUG_MESSAGE("root_configure_request: gdk_window_lookup");
		return GDK_FILTER_CONTINUE;
	}

	value_mask = xconfigurerequest->value_mask &
	    (CWX | CWY | CWWidth | CWHeight | CWBorderWidth);

	values.x = xconfigurerequest->x;
	values.y = xconfigurerequest->y;
	values.width = xconfigurerequest->width;
	values.height = xconfigurerequest->height;
	values.border_width = xconfigurerequest->border_width;
	XConfigureWindow(GDK_DISPLAY(), xid, value_mask, &values);

	return GDK_FILTER_REMOVE;
}


static GdkFilterReturn
root_client_message(GwmRoot * root, GdkEvent * event, XEvent * xevent)
{
	return GDK_FILTER_CONTINUE;
}


static GdkFilterReturn
root_map_request(GwmRoot * root, GdkEvent * event, XEvent * xevent)
{
	guint32 xid;

	xid = xevent->xmaprequest.window;
	gtk_signal_emit(GTK_OBJECT(root), root_signals[MAP_REQUEST], xid);

	return GDK_FILTER_REMOVE;
}


static GdkFilterReturn
root_unmap_notify(GwmRoot * root, GdkEvent * event, XEvent * xevent)
{
	return GDK_FILTER_CONTINUE;
}


GdkFilterReturn
root_filter_func(GdkXEvent * gdk_xevent, GdkEvent * event, gpointer data)
{
	XEvent *xevent;
	GdkFilterReturn return_val;
	GwmRoot *root;

	xevent = (XEvent *) gdk_xevent;
	root = GWM_ROOT(data);

	DEBUG_MESSAGE("root_filter_func: %s", debug_xevent_name(xevent->type));

	switch (xevent->type) {
	case ConfigureRequest:
		return_val = root_configure_request(root, event, xevent);
		break;
	case ClientMessage:
		return_val = root_client_message(root, event, xevent);
		break;
	case MapRequest:
		return_val = root_map_request(root, event, xevent);
		break;
	case UnmapNotify:
		return_val = root_unmap_notify(root, event, xevent);
		break;
	default:
		return_val = GDK_FILTER_CONTINUE;
		break;
	}

	return return_val;
}
