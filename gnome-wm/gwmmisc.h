/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __gwm_misc_h__
#define __gwm_misc_h__

#include <gdk/gdk.h>
#include <gtk/gtktypeutils.h>
#include <gtk/gtkobject.h>
#include "gwmtypes.h"

gboolean gwm_geometry_intersects_area           (GwmGeometry * geometry,
						 GdkRectangle * area);
void     gwm_marshal_NONE__FLOAT_FLOAT          (GtkObject * object,
						 GtkSignalFunc func,
						 gpointer func_data,
						 GtkArg * args);

#endif /* __gwm_misc_h__ */
