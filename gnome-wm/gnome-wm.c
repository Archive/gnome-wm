/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xproto.h>

#include <gdk/gdkprivate.h>
#include <gdk/gdkx.h>
#include <gnome.h>

#include "xatoms.h"
#include "gwmcontext.h"


static const struct poptOption options[] = {
	{NULL, '\0', 0, NULL, 0}
};


int
main(int argc, char *argv[], char **environ)
{
	GwmContext *context;

	/* initiaize GNOME libraries */
	gnome_init_with_popt_table(
	        "gnome-wm", "0.0.1", argc, argv, options, 0, NULL);

	/* get values of X Atoms used within the window manager */
	xatoms_init();

	/* create a context to manage the root window */
	context = gwm_context_new();
	if (!gwm_context_manage_xid(context, GDK_ROOT_WINDOW())) {
		g_error("a window manager is already running");
		gtk_exit(1);
	}
	gwm_context_capture_existing_windows(context);

	gtk_main();
	return 0;
}


