/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GTK_ROOT_H__
#define __GTK_ROOT_H__

#include <gdk/gdk.h>
#include <gtk/gtkcontainer.h>

#define GWM_TYPE_ROOT\
        (gwm_root_get_type())

#define GWM_ROOT(obj)\
        (GTK_CHECK_CAST((obj), GWM_TYPE_ROOT, GwmRoot))

#define GWM_ROOT_CLASS(klass)\
        (GTK_CHECK_CLASS_CAST((klass), GWM_TYPE_ROOT, GwmRootClass))

#define GWM_IS_ROOT(obj)\
        (GTK_CHECK_TYPE((obj), GWM_TYPE_ROOT))

#define GWM_IS_ROOT_CLASS(klass)\
        (GTK_CHECK_CLASS_TYPE((klass), GWM_TYPE_ROOT))

typedef struct _GwmRoot GwmRoot;
typedef struct _GwmRootClass GwmRootClass;
typedef struct _GwmRootChild GwmRootChild;

struct _GwmRoot {
	GtkContainer container;

	GList *children;

	/* viewport/virtual desktops */
	gint viewport_size_x, viewport_size_y;
	gint viewport_offset_x, viewport_offset_y;
};

struct _GwmRootClass {
	GtkContainerClass parent_class;

	void (*map_request) (GwmRoot * root, guint32 xid);
};

struct _GwmRootChild {
	GtkWidget *widget;
	gint layer;
};

GtkType    gwm_root_get_type(void);
GtkWidget *gwm_root_new(GdkWindow * window);
void       gwm_root_add_layer(GwmRoot * root, GtkWidget * widget, gint layer);

#endif				/* __GWM_ROOT_H__ */
