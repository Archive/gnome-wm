/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "xatoms.h"


/* misc atoms */
#define _XA_WM_STATE                    "WM_STATE"
#define _XA_WM_CHANGE_STATE             "WM_CHANGE_STATE"

GdkAtom xa_wm_state;
GdkAtom xa_wm_change_state;


/* WM_PROTOCOLS */
#define _XA_WM_PROTOCOLS                "WM_PROTOCOLS"
#define _XA_WM_TAKE_FOCUS               "WM_TAKE_FOCUS"
#define _XA_WM_SAVE_YOURSELF            "WM_SAVE_YOURSELF"
#define _XA_WM_DELETE_WINDOW            "WM_DELETE_WINDOW"

GdkAtom xa_wm_protocols;
GdkAtom xa_wm_take_focus;
GdkAtom xa_wm_save_yourself;
GdkAtom xa_wm_delete_window;


/* MOTIF WM hints atoms */
#define _XA_MOTIF_BINDINGS		"_MOTIF_BINDINGS"
#define _XA_MOTIF_WM_HINTS		"_MOTIF_WM_HINTS"
#define _XA_MOTIF_WM_MESSAGES		"_MOTIF_WM_MESSAGES"
#define _XA_MOTIF_WM_OFFSET		"_MOTIF_WM_OFFSET"
#define _XA_MOTIF_WM_MENU		"_MOTIF_WM_MENU"
#define _XA_MOTIF_WM_INFO		"_MOTIF_WM_INFO"
#define _XA_MWM_HINTS			_XA_MOTIF_WM_HINTS
#define _XA_MWM_MESSAGES		_XA_MOTIF_WM_MESSAGES
#define _XA_MWM_MENU			_XA_MOTIF_WM_MENU
#define _XA_MWM_INFO			_XA_MOTIF_WM_INFO

GdkAtom xa_motif_bindings;
GdkAtom xa_motif_wm_hints;
GdkAtom xa_motif_wm_messages;
GdkAtom xa_motif_wm_offset;
GdkAtom xa_motif_wm_menu;
GdkAtom xa_motif_wm_info;
GdkAtom xa_mwm_hints;
GdkAtom xa_mwm_messages;
GdkAtom xa_mwm_menu;
GdkAtom xa_mwm_info;


/* GNOME WM protocol atoms */
#define _XA_WIN_PROTOCOLS                "_WIN_PROTOCOLS"
#define _XA_WIN_SUPPORTING_WM_CHECK      "_WIN_SUPPORTING_WM_CHECK"
#define _XA_WIN_CLIENT_LIST              "_WIN_CLIENT_LIST"
#define _XA_WIN_WM_NAME                  "_WIN_WM_NAME"
#define _XA_WIN_WM_VERSION               "_WIN_WM_VERSION"
#define _XA_WIN_AREA                     "_WIN_AREA"
#define _XA_WIN_AREA_COUNT               "_WIN_AREA_COUNT"
#define _XA_WIN_ICONS                    "_WIN_ICONS"
#define _XA_WIN_WORKSPACE                "_WIN_WORKSPACE"
#define _XA_WIN_WORKSPACE_COUNT          "_WIN_WORKSPACE_COUNT"
#define _XA_WIN_WORKSPACE_NAMES          "_WIN_WORKSPACE_NAMES"
#define _XA_WIN_WORKAREA                 "_WIN_WORKAREA"
#define _XA_WIN_LAYER                    "_WIN_LAYER"
#define _XA_WIN_STATE                    "_WIN_STATE"
#define _XA_WIN_HINTS                    "_WIN_HINTS"
#define _XA_WIN_APP_STATE                "_WIN_APP_STATE"
#define _XA_WIN_EXPANDED_SIZE            "_WIN_EXPANDED_SIZE"
#define _XA_WIN_WORKSPACE                "_WIN_WORKSPACE"
#define _XA_WIN_CLIENT_MOVING            "_WIN_CLIENT_MOVING"

GdkAtom xa_win_protocols;
GdkAtom xa_win_supporting_wm_check;
GdkAtom xa_win_client_list;
GdkAtom xa_win_wm_name;
GdkAtom xa_win_wm_version;
GdkAtom xa_win_area;
GdkAtom xa_win_area_count;
GdkAtom xa_win_icons;
GdkAtom xa_win_workspace;
GdkAtom xa_win_workspace_count;
GdkAtom xa_win_workspace_names;
GdkAtom xa_win_workarea;
GdkAtom xa_win_layer;
GdkAtom xa_win_state;
GdkAtom xa_win_hints;
GdkAtom xa_win_app_state;
GdkAtom xa_win_expanded_size;
GdkAtom xa_win_workspace;
GdkAtom xa_win_client_moving;



#define SET_ATOM(atom, string) atom = gdk_atom_intern(string, FALSE)


void
xatoms_init()
{
	/* misc atoms */
	SET_ATOM(xa_wm_state, _XA_WM_STATE);
	SET_ATOM(xa_wm_change_state, _XA_WM_CHANGE_STATE);


	/* WM_PROTOCOLS */
	SET_ATOM(xa_wm_protocols, _XA_WM_PROTOCOLS);
	SET_ATOM(xa_wm_take_focus, _XA_WM_TAKE_FOCUS);
	SET_ATOM(xa_wm_save_yourself, _XA_WM_SAVE_YOURSELF);
	SET_ATOM(xa_wm_delete_window, _XA_WM_DELETE_WINDOW);


	/* MOTIF WM hints atoms */
	SET_ATOM(xa_motif_bindings, _XA_MOTIF_BINDINGS);
	SET_ATOM(xa_motif_wm_hints, _XA_MOTIF_WM_HINTS);
	SET_ATOM(xa_motif_wm_messages, _XA_MOTIF_WM_MESSAGES);
	SET_ATOM(xa_motif_wm_offset, _XA_MOTIF_WM_OFFSET);
	SET_ATOM(xa_motif_wm_menu, _XA_MOTIF_WM_MENU);
	SET_ATOM(xa_motif_wm_info, _XA_MOTIF_WM_INFO);
	SET_ATOM(xa_mwm_hints, _XA_MWM_HINTS);
	SET_ATOM(xa_mwm_messages, _XA_MWM_MESSAGES);
	SET_ATOM(xa_mwm_menu, _XA_MWM_MENU);
	SET_ATOM(xa_mwm_info, _XA_MWM_INFO);
       

	/* GNOME WM protocol atoms */
	SET_ATOM(xa_win_protocols, _XA_WIN_PROTOCOLS);
	SET_ATOM(xa_win_supporting_wm_check, _XA_WIN_SUPPORTING_WM_CHECK);
	SET_ATOM(xa_win_client_list, _XA_WIN_CLIENT_LIST);
	SET_ATOM(xa_win_wm_name, _XA_WIN_WM_NAME);
	SET_ATOM(xa_win_wm_version, _XA_WIN_WM_VERSION);
	SET_ATOM(xa_win_area, _XA_WIN_AREA);
	SET_ATOM(xa_win_area_count, _XA_WIN_AREA_COUNT);
	SET_ATOM(xa_win_icons, _XA_WIN_ICONS);
	SET_ATOM(xa_win_workspace, _XA_WIN_WORKSPACE);
	SET_ATOM(xa_win_workspace_count, _XA_WIN_WORKSPACE_COUNT);
	SET_ATOM(xa_win_workspace_names, _XA_WIN_WORKSPACE_NAMES);
	SET_ATOM(xa_win_workarea, _XA_WIN_WORKAREA);
	SET_ATOM(xa_win_layer, _XA_WIN_LAYER);
	SET_ATOM(xa_win_state, _XA_WIN_STATE);
	SET_ATOM(xa_win_hints, _XA_WIN_HINTS);
	SET_ATOM(xa_win_app_state, _XA_WIN_APP_STATE);
	SET_ATOM(xa_win_expanded_size, _XA_WIN_EXPANDED_SIZE);
	SET_ATOM(xa_win_workspace, _XA_WIN_WORKSPACE);
	SET_ATOM(xa_win_client_moving, _XA_WIN_CLIENT_MOVING);
}
