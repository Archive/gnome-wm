/* gnome-wm
 * Copyright (C) 1999 Jay Painter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *(at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "debug.h"
#include "gwmmisc.h"
#include "gwmhints.h"

#include "standard-hints.h"
#include "motif-hints.h"
#include "gnome-hints.h"
#include "kde-hints.h"


static void gwm_hints_class_init(GwmHintsClass * class);
static void gwm_hints_init(GwmHints * hints);
static void gwm_hints_finalize(GtkObject * object);

static GtkObjectClass *parent_class = NULL;

enum {
	TITLE_CHANGED,
	TAKE_FOCUS_CHANGED,
	SAVE_YOURSELF_CHANGED,
	DELETE_WINDOW_CHANGED,
	ICON_TITLE_CHANGED,
	SIZE_CHANGED,
	POSITION_CHANGED,
	BORDER_WIDTH_CHANGED,
	MIN_SIZE_CHANGED,
	MAX_SIZE_CHANGED,
	SIZE_INCRIMENT_CHANGED,
	ASPECT_RATIO_CHANGED,
	MAXIMIZED_SIZE_CHANGED,
	DECORATION_CHANGED,
	LAST_SIGNAL
};

static guint hints_signals[LAST_SIGNAL] = {0};


GtkType
gwm_hints_get_type(void)
{
	static GtkType hints_type = 0;

	if (!hints_type) {
		static const GtkTypeInfo hints_info = {
			"GwmHints",
			sizeof(GwmHints),
			sizeof(GwmHintsClass),
			(GtkClassInitFunc) gwm_hints_class_init,
			(GtkObjectInitFunc) gwm_hints_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
		
		hints_type = gtk_type_unique(
			gtk_object_get_type(), &hints_info);
	}

	return hints_type;
}


static void
gwm_hints_class_init(GwmHintsClass * class)
{
	GtkObjectClass *object_class;

	parent_class = gtk_type_class(gtk_object_get_type());
	object_class = (GtkObjectClass *) class;

	/* signal initalization */
	hints_signals[TAKE_FOCUS_CHANGED] = gtk_signal_new(
		"take_focus_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, take_focus_changed),
		gtk_marshal_NONE__BOOL,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_BOOL);

	hints_signals[SAVE_YOURSELF_CHANGED] = gtk_signal_new(
		"save_yourself_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, save_yourself_changed),
		gtk_marshal_NONE__BOOL,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_BOOL);


	hints_signals[DELETE_WINDOW_CHANGED] = gtk_signal_new(
		"delete_window_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, delete_window_changed),
		gtk_marshal_NONE__BOOL,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_BOOL);

	hints_signals[TITLE_CHANGED] = gtk_signal_new(
		"title_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, title_changed),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_POINTER);

	hints_signals[ICON_TITLE_CHANGED] = gtk_signal_new(
		"icon_title_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, icon_title_changed),
		gtk_marshal_NONE__POINTER,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_POINTER);

	hints_signals[SIZE_CHANGED] = gtk_signal_new(
		"size_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, size_changed),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	hints_signals[POSITION_CHANGED] = gtk_signal_new(
		"position_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, position_changed),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	hints_signals[BORDER_WIDTH_CHANGED] = gtk_signal_new(
		"border_width_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, border_width_changed),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_INT);

	hints_signals[MIN_SIZE_CHANGED] = gtk_signal_new(
		"min_size_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, min_size_changed),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	hints_signals[MAX_SIZE_CHANGED] = gtk_signal_new(
		"max_size_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, max_size_changed),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

	hints_signals[SIZE_INCRIMENT_CHANGED] = gtk_signal_new(
		"size_incriment_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, size_incriment_changed),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);

 	hints_signals[ASPECT_RATIO_CHANGED] = gtk_signal_new(
		"aspect_ratio_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, aspect_ratio_changed),
		gwm_marshal_NONE__FLOAT_FLOAT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_FLOAT, GTK_TYPE_FLOAT);
	
 	hints_signals[MAXIMIZED_SIZE_CHANGED] = gtk_signal_new(
		"maximized_size_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, maximized_size_changed),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 2,
		GTK_TYPE_INT, GTK_TYPE_INT);
	
 	hints_signals[DECORATION_CHANGED] = gtk_signal_new(
		"decoration_changed",
		GTK_RUN_LAST,
		object_class->type,
		GTK_SIGNAL_OFFSET(GwmHintsClass, decoration_changed),
		gtk_marshal_NONE__INT_INT,
		GTK_TYPE_NONE, 1,
		GTK_TYPE_UINT);

	gtk_object_class_add_signals(
		object_class, hints_signals, LAST_SIGNAL);

	object_class->finalize = gwm_hints_finalize;
}


static void
gwm_hints_init(GwmHints * hints)
{
	hints->take_focus = FALSE;
	hints->save_yourself = FALSE;
	hints->delete_window = FALSE;

	hints->title = NULL;
	hints->icon_title = NULL;

	hints->x = 0;
	hints->y = 0;
	hints->width = 1;
	hints->height = 1;
	hints->border_width = 0;

	hints->min_width = 1;
	hints->min_height = 1;
	hints->max_width = 1024;
	hints->max_height = 768;
	hints->width_inc = 1;
	hints->height_inc = 1;
	hints->min_aspect = 0.0;
	hints->max_aspect = 32768.0;
       
	hints->decoration_flags = 
		GWM_HINTS_DECOR_BORDER |
		GWM_HINTS_DECOR_RESIZEH |
		GWM_HINTS_DECOR_TITLE |
		GWM_HINTS_DECOR_MENU |
		GWM_HINTS_DECOR_MINIMIZE |
		GWM_HINTS_DECOR_MAXIMIZE;
}


static void
gwm_hints_finalize(GtkObject * object)
{
	GwmHints *hints;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GWM_IS_HINTS(object));

	hints = GWM_HINTS(object);

	if (hints->title)
		g_free(hints->title);
	if (hints->icon_title)
		g_free(hints->icon_title);

	(* GTK_OBJECT_CLASS(parent_class)->finalize)(object);
}


GwmHints* 
gwm_hints_new()
{
	GwmHints *hints;
	hints = gtk_type_new(gwm_hints_get_type());
	return hints;
}


void
gwm_hints_load(GwmHints * hints, Window xwindow)
{
	standard_hints_load(hints, xwindow);
	motif_hints_load(hints, xwindow);
	gnome_hints_load(hints, xwindow);
	kde_hints_load(hints, xwindow);
}


gboolean
gwm_hints_process_property_notify(GwmHints * hints, XEvent * xevent)
{
	if (standard_hints_process_client_property(hints, xevent))
		return TRUE;
	if (motif_hints_process_client_property(hints, xevent))
		return TRUE;
	if (gnome_hints_process_client_property(hints, xevent))
		return TRUE;
	if (kde_hints_process_client_property(hints, xevent))
		return TRUE;

	return FALSE;
}


void
gwm_hints_set_take_focus(GwmHints * hints, gboolean take_focus)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->take_focus = take_focus;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[TAKE_FOCUS_CHANGED],
			take_focus);
}


void
gwm_hints_set_save_yourself(GwmHints * hints, gboolean save_yourself)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));
	
	hints->save_yourself = save_yourself;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[SAVE_YOURSELF_CHANGED],
			save_yourself);
}


void
gwm_hints_set_delete_window(GwmHints * hints, gboolean delete_window)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->delete_window = delete_window;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[DELETE_WINDOW_CHANGED],
			delete_window);
}


void
gwm_hints_set_title(GwmHints * hints, gchar * title)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	if (hints->title)
		g_free(hints->title);

	hints->title = g_strdup(title);

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[TITLE_CHANGED], 
			title);
}


void
gwm_hints_set_icon_title(GwmHints * hints, gchar * title)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	if (hints->icon_title)
		g_free(hints->icon_title);

	hints->icon_title = g_strdup(title);

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[ICON_TITLE_CHANGED], 
			title);
}


void
gwm_hints_set_size(GwmHints * hints, gint width, gint height)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->width = width;
	hints->height = height;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[SIZE_CHANGED], 
			width, 
			height);
}


void
gwm_hints_set_position(GwmHints * hints, gint x, gint y)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->x = x;
	hints->y = y;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[POSITION_CHANGED], 
			x, 
			y);
}


void
gwm_hints_set_border_width(GwmHints * hints, gint border_width)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->border_width = border_width;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[BORDER_WIDTH_CHANGED], 
			border_width);
}


void
gwm_hints_set_min_size(GwmHints * hints, gint min_width, gint min_height)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->min_width = min_width;
	hints->min_height = min_height;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[MIN_SIZE_CHANGED], 
			min_width, 
			min_height);
}


void
gwm_hints_set_max_size(GwmHints * hints, gint max_width, gint max_height)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->max_width = max_width;
	hints->max_height = max_height;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[MAX_SIZE_CHANGED], 
			max_width, 
			max_height);
}


void
gwm_hints_set_size_incriment(GwmHints * hints, gint width_inc, gint height_inc)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->width_inc = width_inc;
	hints->height_inc = height_inc;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[SIZE_INCRIMENT_CHANGED], 
			width_inc, 
			height_inc);
}


void
gwm_hints_set_aspect_ratio(GwmHints * hints, 
			   gfloat min_aspect, gfloat max_aspect)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->min_aspect = min_aspect;
	hints->max_aspect = max_aspect;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[ASPECT_RATIO_CHANGED], 
			min_aspect, 
			max_aspect);
}


void
gwm_hints_set_maximized_size(GwmHints * hints,
			     gint maximized_width, gint maximized_height)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->maximized_width = maximized_width;
	hints->maximized_height = maximized_height;

	gtk_signal_emit(GTK_OBJECT(hints),
			hints_signals[MAXIMIZED_SIZE_CHANGED], 
			maximized_width, 
			maximized_height);
}


void
gwm_hints_set_decoration_flags(GwmHints * hints, guint decoration_flags)
{
	g_return_if_fail(hints != NULL);
	g_return_if_fail(GWM_IS_HINTS(hints));

	hints->decoration_flags = decoration_flags;
	gtk_signal_emit(GTK_OBJECT(hints), 
			hints_signals[DECORATION_CHANGED],
			decoration_flags);
}
